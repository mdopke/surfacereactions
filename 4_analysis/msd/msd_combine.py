#!/usr/bin/env

import argparse
import MDAnalysis as mda
from maicos import density_planar, msd_planar
#from PyMD.init_plotting import init_plotting
#init_plotting()
import matplotlib.pyplot as plt
import numpy as np

def read_file(filename, delimiter=' '):

    if filename.find('csv') != -1:
        delimiter = ','

    header = []
    data = []

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#'):
                header.append(line)
            else:
                data.append([float(elem) for elem in line.rstrip('\n').split(delimiter)])

    return np.asarray(data), header

plt.style.use('seaborn-colorblind')
plt.rcParams['font.serif'] = "Georgia"
plt.rcParams['font.family'] = "serif"

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--f', type=str)
    parser.add_argument('--suffix', type=str, default='')
    parser.add_argument('--nstart', type=int)
    parser.add_argument('--nend', type=int)
    parser.add_argument('--nstep', type=int)
    parser.add_argument('--nbins', type=int)
    parser.add_argument('--nframes', type=int)

    args = parser.parse_args()

    # inputs
    # f = '/scratch-shared/mfdopke/simulations/1234/1e0/'
    # nstart = 100000
    # nstep = 10
    # nend = 200000
    # nbins = 5
    # nlen = 10000 

    # calculate Ow msd
    for type in [1, 12, 13]:
        dens = np.zeros((args.nbins,args.nframes))
        msdx = np.zeros((args.nbins,args.nframes))
        msdy = np.zeros((args.nbins,args.nframes))
        msdz = np.zeros((args.nbins,args.nframes))
        for i in range(args.nstart, args.nend, args.nstep):
            print('%i\r' % i, end='')
            try:
                dens_, _ = read_file(args.f + '/msd'+str(args.suffix)+'/type_'+str(type)+'_'+str(i)+'_dens.dat')
                msdx_, _ = read_file(args.f + '/msd'+str(args.suffix)+'/type_'+str(type)+'_'+str(i)+'_msdx.dat')
                msdy_, _ = read_file(args.f + '/msd'+str(args.suffix)+'/type_'+str(type)+'_'+str(i)+'_msdy.dat')
                msdz_, _ = read_file(args.f + '/msd'+str(args.suffix)+'/type_'+str(type)+'_'+str(i)+'_msdz.dat')
                for j in range(args.nbins):
                    dens[j, 0:args.nframes] += dens_[j, 1:]
                    msdx[j, 0:args.nframes] += msdx_[j, 1:]
                    msdy[j, 0:args.nframes] += msdy_[j, 1:]
                    msdz[j, 0:args.nframes] += msdz_[j, 1:]
            except:
                break

        z = dens[:, 0]

        msdx /= dens
        msdy /= dens
        msdz /= dens

        np.savetxt(args.f + '/data/type_'+str(type)+'_msdx.dat'+str(args.suffix), np.hstack([z[:, np.newaxis], msdx[:, :]]), header='msdx')
        np.savetxt(args.f + '/data/type_'+str(type)+'_msdy.dat'+str(args.suffix), np.hstack([z[:, np.newaxis], msdy[:, :]]), header='msdy')
        np.savetxt(args.f + '/data/type_'+str(type)+'_msdz.dat'+str(args.suffix), np.hstack([z[:, np.newaxis], msdz[:, :]]), header='msdz')
