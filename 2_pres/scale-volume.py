#!/bin/bash

import sys
import numpy as np
import argparse

from PyMD.IO import read_lammpsdata, write_lammpsdata, read_lammpstrj
from PyMD.System import System, Box

from MDAnalysis import Universe


def read_file(filename):

    header = []
    data = []

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#'):
                header.append(line)
            else:
                data.append([float(elem) for elem in line.rstrip('\n').split(' ')])

    return np.asarray(data), header


if __name__ == "__main__":

    # inputs that need defining
    parser = argparse.ArgumentParser()

    parser.add_argument('--fdat', type=str)
    parser.add_argument('--ftrj', type=str)
    parser.add_argument('--fout', type=str)

    parser.add_argument('--nstart', type=int, default=0)
    parser.add_argument('--nend', type=int, default=-1)
    parser.add_argument('--nevery', type=int, default=1)

    args = parser.parse_args()

    # -------------------------------------------------------
    # use PyMD
    top = read_lammpsdata(args.fdat)

    left = np.where(top.pos[:, 2] < 0)[0]
    right = np.where(top.pos[:, 2] > 0)[0]
 
    surface = np.where(
            (top.types == 3) |
            (top.types == 4) |
            (top.types == 5) |
            (top.types == 6) |
            (top.types == 7) |
            (top.types == 8) |
            (top.types == 9) |
            (top.types == 10) |
            (top.types == 11))[0]
 
    fluid = np.where(
            (top.types == 1) |
            (top.types == 2) |
            (top.types > 11))[0]
 
    # intersect
    left_surface = list(set(surface) & set(left))
    right_surface = list(set(surface) & set(right))

    # -------------------------------------------------------
    # use MD analysis to read data and convert to PyMD
    u = Universe(args.fdat, args.ftrj, topology_format='DATA')

    # -------------------------------------------------------
    # selection
    selection = np.where((top.types == 1) | (top.types == 2))[0]

    # -------------------------------------------------------
    # analyze
    progress = 0
    nframes = len(u.trajectory)
    if args.nend == -1:
        args.nend = nframes+1
    counter = 0

    avgd = 0

    for key, ts in enumerate(u.trajectory):

        # -------------------------------------------------------

        progress += 1 / nframes * 100
        print('%i %% %d/%d\r' % (progress, key, nframes), end='')

        # -------------------------------------------------------

        atoms = u.select_atoms('all')

        if key >= args.nstart and (key - args.nstart) % args.nevery == 0 and key < args.nend:

            counter += 1

            trj = System()
            trj.pos = atoms.positions
            trj.box.lengths = atoms.dimensions[:3]
            trj.box.mins = -trj.box.lengths / 2
            trj.box.maxs = trj.box.lengths / 2

            trj.molids = top.molids
            trj.charges = top.charges

            # -------------------------------------------------------

            # wrap and r molecules accross PBC
            trj.wrap()
            bbox = trj.bounding_box(selection)
            avgd += bbox.lengths[2]
    print()


    avgd /= counter
    d = bbox.lengths[2]

    print('Average d %.4f' % avgd)
    print('Last d %.4f' % d)

    # write last time step to system
    top.pos = trj.pos

    # shift walls
    diff = avgd - d

    top.pos[left_surface, 2] -= diff/2
    top.pos[right_surface, 2] += diff/2

    # scale fluid
    fac = avgd/d

    top.pos[fluid, 2] *= fac

    # adjust box size
    # find min and max z
    zmax = np.max(np.abs(top.pos[:, 2]))
    top.box.mins[2] = -zmax - 2
    top.box.maxs[2] = zmax + 2
    top.box.lengths[2] = top.box.maxs[2] - top.box.mins[2]

    write_lammpsdata(args.fout, top, style='default')


