###################################################

units real
dimension 3
newton on
boundary p p f
atom_style full

#-----------------INPUT-----------------#
read_data in.lammpsdata
include in.forcefield

#################### SET SURFACE CHARGE ####################

variable numSC loop 16        # Number of deprotonated sites due to surface charge
variable qHd equal 0
variable qOd equal -0.9
variable qSid equal 0.725
variable Hsc file inputs/H_surfacecharge.dat	# files with lists of atom IDs
variable Osc file inputs/O_surfacecharge.dat
variable Sisc file inputs/Si_surfacecharge.dat
#LOOP
label loop1
set atom ${Hsc} charge ${qHd}	# set atom charges to deprotonated
set atom ${Osc} charge ${qOd}
set atom ${Sisc} charge ${qSid}
next numSC			# take next atoms in list and next deprotonated site number
next Hsc Osc Sisc
jump in.simulation loop1	#jump back up to start loop again

#################### settings ####################

reset_timestep 0 # set timestep counter to x

variable Nprod equal 80000000 		# 10 ns
variable transittime equal 1000 

variable reacttime equal 134300

variable Nthermo equal 2000    	# every 1 ps give themodata
variable Ndump equal 400

#---------------------------------------#

variable Temp equal 298       # Temperature in K
variable Pres equal 1.0       # Pressure in atm.
variable tstep equal 1.0      # 1fs

run_style verlet

neighbor 2.0 bin
neigh_modify every 1 delay 0 check yes 	# rebuilt list every step, do not delay and rebuilt if an atom has moved half the skin distance or more

thermo_style one
thermo ${Nthermo}

dump VMD all dcd ${Ndump} out.dcd 	# every 1 ps dump trj
dump_modify VMD unwrap yes

#################### INITIALIZATION ####################

print ""
print "FIXES"
print ""

timestep ${tstep}

variable Lz equal (zhi-8)

region BLOCK1 block -1000 1000 -1000 1000 -1000 $(-v_Lz)
region BLOCK2 block -1000 1000 -1000 1000 $(v_Lz) 1000

group REGION1 region BLOCK1
group REGION2 region BLOCK2

group RIGID type 3 4
group RIGID1 intersect RIGID REGION1
group RIGID2 intersect RIGID REGION2

# ################ NORMAL ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 1 2 3 4 5 6 7 8 9 10 11 12 13
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
################ VISCOSITY ####################

group SHAKE type 1 2 8 11
group ALL type 1 2 3 4 5 6 7 8 9 10 11 12 13
group SIM subtract ALL RIGID1 RIGID2
group FLUID type 1 2 12 13

fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)

fix move1 RIGID1 move linear 0.0002 0 0
fix move2 RIGID2 move linear -0.0002 0 0

compute f1 RIGID1 reduce sum fx
compute f2 RIGID2 reduce sum fx

fix write all ave/time 1 ${Ndump} ${Ndump} c_f1 c_f2 file out.forces

# ################ EMOSIS ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 3 4 5 6 7 8 9 10 11
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
# fix 2 FLUID nve
# fix kick FLUID efield 0.02 0.0 0.0
#
# ################ Poiseuille ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 3 4 5 6 7 8 9 10 11
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
# fix 2 FLUID nve
# fix kick FLUID gravity 0.00005155942 vector 1 0 0 # equivalent to 75 atm in our system

################ SIMULATION ####################

print ""
print "SETTING VARIABLES"
print ""

variable statictime equal ${reacttime}-${transittime} 		# time between end of transit time and next reaction
variable numreactionsint equal ceil(${Nprod}/${reacttime})	# number of events
variable numreactions loop ${numreactionsint}

variable qHp equal 0.4		# partrial charges
variable qHd equal 0
variable qOp equal -0.675
variable qOd equal -0.9
variable qSip equal 1.1	
variable qSid equal 0.725
variable qSidd equal 0.35

variable Hpl file inputs/which_H_protonates.left	# files with lists of atom IDs
variable Opl file inputs/which_O_protonates.left
variable Sipl file inputs/which_Si_protonates.left
variable Cpl file inputs/charge_of_protonating_Si.left
variable Hdl file inputs/which_H_deprotonates.left
variable Odl file inputs/which_O_deprotonates.left
variable Sidl file inputs/which_Si_deprotonates.left
variable Cdl file inputs/charge_of_deprotonating_Si.left
variable Hpr file inputs/which_H_protonates.right
variable Opr file inputs/which_O_protonates.right
variable Sipr file inputs/which_Si_protonates.right
variable Cpr file inputs/charge_of_protonating_Si.right
variable Hdr file inputs/which_H_deprotonates.right
variable Odr file inputs/which_O_deprotonates.right
variable Sidr file inputs/which_Si_deprotonates.right
variable Cdr file inputs/charge_of_deprotonating_Si.right

print ""
print "PRODUCTION"
print ""

#LOOP
label loop2
print "numreactions="
print ${numreactions}

timestep ${tstep}
run ${statictime}	#run with current static surface configuration

variable startstep equal step
variable Htp atom ${qHd}+(${qHp}-${qHd})*(step-${startstep})/${transittime} # set transition variables for H and O for this loop
variable Otp atom ${qOd}+(${qOp}-${qOd})*(step-${startstep})/${transittime} 
variable Htd atom ${qHp}+(${qHd}-${qHp})*(step-${startstep})/${transittime} 
variable Otd atom ${qOp}+(${qOd}-${qOp})*(step-${startstep})/${transittime} 
if "${Cpl} == ${qSip}" then &
	"variable Sitpl atom ${qSid}+(${qSip}-${qSid})*(step-${startstep})/${transittime}" &
elif "${Cpl} == ${qSid}" &
	"variable Sitpl atom ${qSidd}+(${qSid}-${qSidd})*(step-${startstep})/${transittime}" # set charge variables for Si for this loop
if "${Cdl} == ${qSid}" then &
	"variable Sitdl atom ${qSip}+(${qSid}-${qSip})*(step-${startstep})/${transittime}" &
elif "${Cdl} == ${qSidd}" &
	"variable Sitdl atom ${qSid}+(${qSidd}-${qSid})*(step-${startstep})/${transittime}"
if "${Cpr} == ${qSip}" then &
	"variable Sitpr atom ${qSid}+(${qSip}-${qSid})*(step-${startstep})/${transittime}" &
elif "${Cpr} == ${qSid}" &
	"variable Sitpr atom ${qSidd}+(${qSid}-${qSidd})*(step-${startstep})/${transittime}"
if "${Cdr} == ${qSid}" then &
	"variable Sitdr atom ${qSip}+(${qSid}-${qSip})*(step-${startstep})/${transittime}" &
elif "${Cdr} == ${qSidd}" &
	"variable Sitdr atom ${qSid}+(${qSidd}-${qSid})*(step-${startstep})/${transittime}"
set atom ${Hpl} charge v_Htp
set atom ${Opl} charge v_Otp
set atom ${Hdl} charge v_Htd
set atom ${Odl} charge v_Otd
set atom ${Hpr} charge v_Htp
set atom ${Opr} charge v_Otp
set atom ${Hdr} charge v_Htd
set atom ${Odr} charge v_Otd
set atom ${Sipl} charge v_Sitpl
set atom ${Sidl} charge v_Sitdl
set atom ${Sipr} charge v_Sitpr
set atom ${Sidr} charge v_Sitdr

run ${transittime} pre no post no	# run for transittime to ramp up/down charges

set atom ${Hpl} charge ${qHp}		# when they are close enough set charges to static values
set atom ${Opl} charge ${qOp}
set atom ${Sipl} charge ${Cpl}
set atom ${Hdl} charge ${qHd}
set atom ${Odl} charge ${qOd}
set atom ${Sidl} charge ${Cdl}
set atom ${Hpr} charge ${qHp}
set atom ${Opr} charge ${qOp}
set atom ${Sipr} charge ${Cpr}
set atom ${Hdr} charge ${qHd}
set atom ${Odr} charge ${qOd}
set atom ${Sidr} charge ${Cdr}

variable startstep delete
next numreactions	# take next atoms that react from lists and next reaction number
next Hpl Opl Sipl Hdl Odl Sidl Hpr Opr Sipr Hdr Odr Sipr Cpl Cdl Cpr Cdr

jump in.simulation loop2	# jump back up to start loop again

print ""
print "OUTPUT"
print ""

write_data out.lammpsdata nocoeff

quit

