<h2>README for </h2>

citation

<h2>Folder structure</h2>

* 1_prepare-files -> contains methodology

    + define-reactions        -> python routines selecting sites to react (initial data file required)

        - 1_Initialization    -> select initial silanols to deprotonate and write to list

        - 2_Production        -> select silanols to deprotonate and protonate at every reaction

    + lammpsdata_files        -> data files of the simulation

    + lammpssimulation_files  -> simulation files with loop to execute reactions. Reads outputs from "define-reactions" and adjusts charges of surface groups using a loop

        - bulk        -> bulk simulations

        - emosis      -> electro-osmotic simulations

        - poiseuille  -> poiseuille flow simulations (streaming currents)

        - shear       -> shearing walls to determine local viscosity profile

* 2_pres          -> adjusts fluid density

* 3_simulations   -> initial simulation files

* 4_analysis      -> post-processing routines for properties: density profile, velocity profile, water orientation and mean squared displacements

* 5_results       -> results

<h5>asmbox and pymd are provided at the version the project was carried out.

<h5>Latest versions can be found at https://gitlab.com/mdopke/


