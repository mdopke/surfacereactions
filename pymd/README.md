#!/bin/bash

Author:
Max Dopke
m.f.dopke@tudelft.nl

PyMD is a package designed to read, write and manipulate LAMMPS data files

It also contains functionality for some force fields and lammps simulation files. These should be used with care and only by experienced users.

# ------------------------------------------------------------------------ #
Installation instructions

# Install git
sudo apt-get install git

# set up git (Not necessary if you do not wish to commit)
mkdir <directory>
cd <directory>
git init

# clone the repository
git clone https://gitlab.com/mdopke/pymd.git

Add PyMD to you path. 
I use anaconda and simply use a soft link
ln -s PyMD anaconda3/lib/python3.5/site-packages 
make sure to write the full path

# ------------------------------------------------------------------------ #
DESCRIPTION OF FILES

# ------------------------------------------------------------------------ #
System.py 
This function is the workhorse of PyMD. It contains the classes Box, Atom and System which provide efficient storage of data and allow easy access and manipulation of data.

# ------------------------------------------------------------------------ #
IO.py
IO stands for InputOuput. This function provides the necessary routines to read and write LAMMPS data and trajectory files.

# ------------------------------------------------------------------------ #
functions.py

# ------------------------------------------------------------------------ #
force_fields.py
Experienced users can easily write force fields.

The functionality of this file is heavily dependent on the force fields specified in FORCEFIELDS

# ------------------------------------------------------------------------ #
init_plotting.py
Plot settings.

# ------------------------------------------------------------------------ #
tools/lammpstrj_to_lammpsdata.py
This tool allows to write a LAMMPS trajectory frame to a LAMMPS data file

# ------------------------------------------------------------------------ #
tools/write_LAMMPSDATA_water.py
This tool allows the creation of a simple LAMMPS data file containing water molecules.

Required h2o.txt which contains a sample water molecule.

# ------------------------------------------------------------------------ #
tools/write_LAMMPSDATA_ions.py
This tool allows for the addition or ions to a LAMMPS data file. Implicit and Explicit solvents are currently supported.

Sample use of toolds
python PyMD/TOOLS/write_LAMMPSDATA_water.py --fout in.lammpsdata --box 20 20 20 --dens 1
python PyMD/TOOLS/write_LAMMPSDATA_ions.py --fin in.lammpsdata --fout in.lammpsdata --box 20 20 20 --ion symb:Na type:3 add:2 --ion symb:Cl type:4 add:2
python PyMD/TOOLS lammpsdatafilein lammpstrjfile lammpsdatafileout
 