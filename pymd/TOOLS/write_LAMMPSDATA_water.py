#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water
from PyMD.System import System, Box
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--style', type=str)
    parser.add_argument('--wrap', type=str, help='True/False', default='True')
    parser.add_argument('--typeO', type=int, default=1)
    parser.add_argument('--typeH', type=int, default=2)
    parser.add_argument('--typeOH', type=int, default=1)
    parser.add_argument('--typeHOH', type=int, default=1)
    parser.add_argument('--dens', type=float)
    parser.add_argument('--box', metavar='N', type=float, nargs='+',
                        help='[xmin, xmax, ymin, ymax, zmin, zmax] or [Lx, Ly, Lz]')
    args = parser.parse_args()
    args.box = numpy.array(args.box)

    if args.style == None:
        args.style = 'default'

    if args.fin != None:
        # read file
        system = read_lammpsdata(args.fin, style=args.style)
    elif args.fin == None:
        system = System()

    if len(args.box) == 3:
        # Lx, Ly, Lz provided
        region = Box()
        region.lengths = args.box
        region.mins = - region.lengths / 2
        region.maxs = region.lengths / 2
    elif len(args.box) == 6:
        # xmin, xmax, ymin, ymax, zmin, zmax provided
        region = Box()
        region.mins = args.box[[0, 2, 4]]
        region.maxs = args.box[[1, 3, 5]]
        region.lengths = region.maxs - region.mins
    else:
        print('Wrong inputs for box provided')
        exit()


    print(region)
    system = add_water(density=args.dens, box=region, system=system, typeO=args.typeO, typeH=args.typeH)
    system.mass_dict.update({args.typeO: 15.9994, args.typeH: 1.008})
    system.type_dict.update({args.typeO: 'O', args.typeH: 'H'})
    if args.fin == None:
        system.box = region
    if args.wrap == 'True':
        system.wrap()

    # settings to start with
    # atoms = {1: 'Ow', 2: 'Hw'}
    # bonds = {1: 'OwHw'}
    # angles = {1: 'HwOwHw'}

    write_lammpsdata(args.fout, system, style=args.style)
