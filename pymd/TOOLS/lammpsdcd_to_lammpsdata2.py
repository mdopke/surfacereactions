#!/usr/bin/env

# load system class

from PyMD.IO import read_lammpsdata, write_lammpsdata
from MDAnalysis import Universe
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--ftrj', type=str)
    parser.add_argument('--style', type=str, default='default')
    parser.add_argument('--frame', type=int, help='frame to use', default=-1)

    args = parser.parse_args()


    data = read_lammpsdata(args.fin, style=args.style)

    # trajectory = read_lammpstrj(lammpstrjfile, read_velocities=False, read_forces=False, max_size=2)
    u = Universe(args.fin, args.ftrj, topology_format='DATA')

    n = len(u.trajectory)
    for key, ts in enumerate(u.trajectory):
        print('Reading frame %i\r' % key, end='')

        atoms = u.select_atoms('all')

        if key == args.frame or (key == n-1 and args.frame == -1):
            data.pos =  atoms.positions
            data.box.lengths = atoms.dimensions[:3]
            data.box.mins = -data.box.lengths / 2
            data.box.maxs = data.box.lengths / 2

    print()

    write_lammpsdata(args.fout, data, vels=False, style=args.style)
