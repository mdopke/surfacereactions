#!/usr/bin/env

import numpy
from PyMD.force_fields import *
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--ff', type=str)

    args = parser.parse_args()

    if not args.ff or (args.ff == 'SPCE' or args.ff == 'SPC/E' or args.ff == 'spce' or args.ff == 'spc/e'):
        args.ff = 'SPCE'

    print('currently only providing SPCE water')

    # define dictionary of what should be in the force field
    atoms = {1: 'Ow', 2: 'Hw'}
    bonds = {1:'OwHw'}
    angles = {1:'HwOwHw'}

    # read force fields
    FF = read_ff(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))) +
                   '/../FORCEFIELDS/water/' + args.ff, bonded=True)

    write_forcefield(args.fout,
                     'lj/cut/coul/long 10.0 10.0', # functional
                     atoms, bonds, angles, FF) # input data
