#!/usr/bin/env

# load system class

import numpy
import math
import copy
import re
import warnings
import os
import psutil
import sys
from MDAnalysis import Universe
import argparse
from PyMD.IO import read_lammpsdata


def write_lammpstrj_frame(xyz, types, vels=None, mols=None, step=1, box=1, fmt='5col',  filename='traj.lammpstrj', mode='a'):
    assert len(xyz) == len(types)
    item_line = {'5col': 'ITEM: ATOMS id type x y z\n'}
    with open(filename, mode) as f:
        f.write('ITEM: TIMESTEP\n')
        f.write('%d\n' % step)
        f.write('ITEM: NUMBER OF ATOMS\n')
        f.write('%d\n' % len(xyz))
        f.write('ITEM: BOX BOUNDS\n')
        for i in range(3):
            f.write('%.6f %.6f\n' % (-box[i]/2, box[i]/2))
        f.write(item_line[fmt])
        for i, pos in enumerate(xyz):
            if fmt == '5col':
                f.write('%d %d %.4f %.4f %.4f\n' %
                        (i+1, types[i], pos[0], pos[1], pos[2]))
            elif fmt == '6col':
                f.write('%d %d %d %.4f %.4f %.4f\n' %
                        (i+1, mols[i], types[i], pos[0], pos[1], pos[2]))
            elif fmt == '9col':
                f.write('%d %d %d %.4f %.4f %.4f %.4f %.4f %.4f\n' %
                        (i+1, mols[i], types[i], pos[0], pos[1], pos[2], vels[i, 0], vels[i, 1], vels[i, 2]))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str, default='/home/mfdopke/Dropbox/Phd/PROJECTS/silica-dissolution/ff-selection/bulk-properties/run/TIP4P2005_interface_Zeron2019/NaCl/Nw_555_Ni_10/in.lammpsdata')
    parser.add_argument('--fout', type=str, default='/home/mfdopke/Dropbox/Phd/PROJECTS/silica-dissolution/ff-selection/bulk-properties/run/TIP4P2005_interface_Zeron2019/NaCl/Nw_555_Ni_10/out.lammpstrj')
    parser.add_argument('--ftrj', type=str, default='/home/mfdopke/Dropbox/Phd/PROJECTS/silica-dissolution/ff-selection/bulk-properties/run/TIP4P2005_interface_Zeron2019/NaCl/Nw_555_Ni_10/out.dcd')
    parser.add_argument('--style', type=str, default='default')

    args = parser.parse_args()


    top = read_lammpsdata(args.fin, style=args.style)
    u = Universe(args.fin, args.ftrj, topology_format='DATA')

    LAST = True
    for key, ts in enumerate(u.trajectory):
        print('Reading frame %i\r' % key, end='')

        atoms = u.select_atoms('all')

        write_lammpstrj_frame(xyz=atoms.positions, types=top.types, mols=top.molids, step=key*5000, box=atoms.dimensions[:3], filename=args.fout)
