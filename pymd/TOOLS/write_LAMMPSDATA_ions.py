#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import minimize
from PyMD.System import System, Box, Atom
import argparse
import ast

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--instyle', type=str)
    parser.add_argument('--outstyle', type=str)
    parser.add_argument('--box', metavar='N', type=float, nargs='+',
                        help='[xmin, xmax, ymin, ymax, zmin, zmax] or [Lx, Ly, Lz]')
    parser.add_argument('--ion', type=str, action='append', nargs='+')

    args = parser.parse_args()

    args.box = numpy.array(args.box)

    if args.instyle == None:
        args.instyle = 'default'

    if args.outstyle == None:
        args.outstyle = 'default'

    if args.fin != None:
        # read file
        system = read_lammpsdata(args.fin, style=args.instyle)
    elif args.fin == None:
        system = System()

    ions = {}
    for ci, i in enumerate(args.ion):

        ions[ci] = {}
        for j in i:
            if j.split(':')[0] == 'symb':
                ions[ci][j.split(':')[0]] = j.split(':')[1]
            elif j.split(':')[0] == 'type':
                ions[ci][j.split(':')[0]] = int(j.split(':')[1])
            elif j.split(':')[0] == 'add':
                ions[ci][j.split(':')[0]] = int(j.split(':')[1])
            else:
                ions[ci][j.split(':')[0]] = float(j.split(':')[1])

        if not 'type' in ions[ci].keys():
            ions[ci]['type'] = system.n_atomTypes() + 1

        if not 'm' in ions[ci].keys():
            ions[ci]['m'] = 1

        if not 'q' in ions[ci].keys():
            ions[ci]['q'] = 1

    if len(args.box) == 3:
        # Lx, Ly, Lz provided
        region = Box()
        region.lengths = args.box
        region.mins = - region.lengths / 2
        region.maxs = region.lengths / 2
    elif len(args.box) == 6:
        # xmin, xmax, ymin, ymax, zmin, zmax provided
        region = Box()
        region.mins = args.box[[0, 2, 4]]
        region.maxs = args.box[[1, 3, 5]]
        region.lengths = region.maxs - region.mins
    else:
        print('Wrong inputs for box provided')
        exit()
    midpoints = (region.maxs + region.mins) / 2


    #system.box = region
    system.wrap()

    for ci in ions.keys():
        Ion = Atom(symb=ions[ci]['symb'], type=ions[ci]['type'], mass=ions[ci]['m'], charge=ions[ci]['q'])
        for cj in range(ions[ci]['add']):
            if len(system.pos) == 0:
                xyz = (numpy.random.rand(3) - 0.5) * region.lengths + midpoints
            else:
                xyz = minimize(system, (numpy.random.rand(3) - 0.5) * region.lengths + midpoints, maxIter=200, region='local')
            system.insert_atom(atom=Ion, pos=xyz)

    system.wrap()

    write_lammpsdata(args.fout, system, style=args.outstyle)
