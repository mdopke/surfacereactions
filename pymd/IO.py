#!/usr/bin/env

import numpy
import re
import warnings
import os
import psutil

from PyMD.System import System

def read_file(filename, delimiter=' '):

    if filename.find('csv') != -1:
        delimiter = ','

    header = []
    data = []

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#'):
                header.append(line)
            else:
                data.append([float(elem) for elem in line.rstrip('\n').split(delimiter)])

    return numpy.asarray(data), header



def read_lammpsdata(filename, style='default'):
    
    # TODO: improve robustness of xlo regex
    directives = re.compile(r"""
        ((?P<n_atoms>\s*\d+\s+atoms)
        |
        (?P<n_bonds>\s*\d+\s+bonds)
        |
        (?P<n_angles>\s*\d+\s+angles)
        |
        (?P<n_dihedrals>\s*\d+\s+dihedrals)
        |
        (?P<n_impropers>\s*\d+\s+impropers)
        |
        (?P<box>.+xlo)
        |
        (?P<Masses>\s*Masses)
        |
        (?P<PairCoeffs>\s*Pair\sCoeffs)
        |
        (?P<BondCoeffs>\s*Bond\sCoeffs)
        |
        (?P<AngleCoeffs>\s*Angle\sCoeffs)
        |
        (?P<DihedralCoeffs>\s*Dihedral\sCoeffs)
        |
        (?P<ImproperCoeffs>\s*Improper\sCoeffs)
        |
        (?P<Atoms>\s*Atoms)
        |
        (?P<Velocities>\s*Velocities)
        |
        (?P<Bonds>\s*Bonds)
        |
        (?P<Angles>\s*Angles)
        |
        (?P<Dihedrals>\s*Dihedrals)
        |
        (?P<Impropers>\s*Impropers))
        """, re.VERBOSE)

    # load empy counters
    n_atoms     = 0
    n_bonds     = 0
    n_angles    = 0
    n_dihedrals = 0
    n_impropers = 0

    # data = System()

    # Read file into lines
    print("Reading '" + filename + "'")
    with open(filename, 'r') as f:
        data_lines = f.readlines()

    # iterate through every line
    i = 0
    while i < len(data_lines):
        match = directives.match(data_lines[i])
        if match:

            # initiate the class
            if match.group('n_atoms'):
                fields = data_lines.pop(i).split()
                n_atoms = int(fields[0])
                # data.update(n_atoms=int(fields[0]))

            elif match.group('n_bonds'):
                fields = data_lines.pop(i).split()
                n_bonds = int(fields[0])
                # data.update(n_bonds=int(fields[0]))

            elif match.group('n_angles'):
                fields = data_lines.pop(i).split()
                n_angles = int(fields[0])
                # data.update(n_angles=int(fields[0]))

            elif match.group('n_dihedrals'):
                fields = data_lines.pop(i).split()
                n_dihedrals = int(fields[0])
                # data.update(n_dihedrals=int(fields[0]))

            elif match.group('n_impropers'):
                fields = data_lines.pop(i).split()
                n_impropers = int(fields[0])
                # data.update(n_impropers=int(fields[0]))

            elif match.group('box'):

                # initiate our atoms class
                data = System(n_atoms=n_atoms, n_bonds=n_bonds, n_angles=n_angles, n_dihedrals=n_dihedrals, n_impropers=n_impropers)

                # write box dimension to class
                for j in range(3):
                    fields = [float(x) for x in data_lines.pop(i).split()[:2]]
                    data.box.mins[j] = fields[0]
                    data.box.maxs[j] = fields[1]

                data.box.lengths = data.box.maxs - data.box.mins
                data.box.centers = (data.box.maxs + data.box.mins) / 2

            elif match.group('Masses'):

                # pop 'Masses'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.mass_dict[int(fields[0])] = float(fields[1])

                    # Add comments to type_dcit
                    if len(fields) == 4:
                        data.type_dict[int(fields[0])] = str(fields[3])

            elif match.group('Atoms'):

                # pop 'Atoms'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    line = data_lines.pop(i).split()

                    # check for comments
                    if any(field == '#' for field in line) == True:
                        commentfield = line.index('#')
                        data.atoms_comments[int(line[0]) - 1] = ' '.join(line[commentfield+1:])
                        fields = line[:commentfield]
                    else:
                        fields = line


                    # possibilities supported
                    # index molid typeid charge xu yu zu
                    # index typeid charge xu yu zu 'for reax'

                    # initiate atom index counter
                    a_id = int(fields[0])
                    if style == 'default':

                        # normal style (no flags supported)
                        if len(fields) == 7:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(fields[1])
                            data.types[a_id - 1] = int(fields[2])
                            data.charges[a_id - 1] = float(fields[3])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[2])]
                            data.pos[a_id - 1] = numpy.array([float(fields[4]), float(fields[5]), float(fields[6])])

                        # normal with flags
                        elif len(fields) == 10:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(fields[1])
                            data.types[a_id - 1] = int(fields[2])
                            data.charges[a_id - 1] = float(fields[3])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[2])]
                            flags = numpy.array([float(fields[7]), float(fields[8]), float(fields[9])])
                            pos = numpy.array([float(fields[4]), float(fields[5]), float(fields[6])])
                            data.pos[a_id - 1] = pos + flags * data.box.lengths

                    elif style == 'reax':
                        # reax style with no molecule id
                        if len(fields) == 6:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(0)
                            data.types[a_id - 1] = int(fields[1])
                            data.charges[a_id - 1] = float(fields[2])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[1])]
                            data.pos[a_id - 1] = numpy.array([float(fields[3]), float(fields[4]), float(fields[5])])

                            # reax style with no molecule id, with flags
                        elif len(fields) == 9:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(0)
                            data.types[a_id - 1] = int(fields[1])
                            data.charges[a_id - 1] = float(fields[2])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[1])]
                            data.pos[a_id - 1] = numpy.array([float(fields[3]), float(fields[4]), float(fields[5])])




            elif match.group('Velocities'):

                # pop 'Velocities'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.vels[int(fields[0]) - 1] = numpy.array([float(fields[1]), float(fields[2]), float(fields[3])])

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS=True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS=False

                    if COMMENTS:
                        try:
                            data.vels_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield+1:])
                        except:
                            pass


            elif match.group('Bonds'):

                # pop 'Bonds'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.bonds[int(fields[0]) - 1] = list(map(int, fields[1:4]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS=True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS=False

                    if COMMENTS:
                        try:
                            data.bonds_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield+1:])
                        except:
                            pass


            elif match.group('Angles'):

                # pop 'Angles'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.angles[int(fields[0]) - 1] = list(map(int, fields[1:5]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS=True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS=False

                    if COMMENTS:
                        try:
                            data.angles_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield+1:])
                        except:
                            pass

            elif match.group('Dihedrals'):

                # pop 'Dihedrals'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.dihedrals[int(fields[0]) - 1] = list(map(int, fields[1:6]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS=True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS=False

                    if COMMENTS:
                        try:
                            data.dihedrals_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield+1:])
                        except:
                            pass

            elif match.group('Impropers'):

                # pop 'Impropers'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.impropers[int(fields[0]) - 1] = list(map(int, fields[1:7]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS=True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS=False

                    if COMMENTS:
                        try:
                            data.impropers_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield+1:])
                        except:
                            pass

            else:
                i += 1
        else:
            i += 1

    return data



def write_lammpsdata(filename, data, style='default', vels=False, masses=True):

    with open(filename, 'w') as f:
        f.write('LAMMPS data file generated with write_lammpsdata from private PyMD library. Ask author for python script m.f.dopke@tudelft.nl\n')
        f.write('\n')

        # write counters
        f.write(str(data.n_atoms()) + ' atoms\n')
        if style == 'default':
            f.write(str(data.n_bonds()) + ' bonds\n')
            f.write(str(data.n_angles()) + ' angles\n')
            f.write(str(data.n_dihedrals()) + ' dihedrals\n')
            f.write(str(data.n_impropers()) + ' impropers\n')
        f.write('\n')

        f.write(str(data.n_atomTypes()) + ' atom types\n')
        if style == 'default':
            if data.n_bonds() > 0:
                f.write(str(data.n_bondTypes()) + ' bond types\n')
            if data.n_angles() > 0:
                f.write(str(data.n_angleTypes()) + ' angle types\n')
            if data.n_dihedrals() > 0:
                f.write(str(data.n_dihedralTypes()) + ' dihedral types\n')
            if data.n_impropers() > 0:
                f.write(str(data.n_improperTypes()) + ' improper types\n')
        f.write('\n')

        # write box
        f.write('% 8.4f % 8.4f xlo xhi\n' % (data.box.mins[0], data.box.maxs[0]))
        f.write('% 8.4f % 8.4f ylo yhi\n' % (data.box.mins[1], data.box.maxs[1]))
        f.write('% 8.4f % 8.4f zlo zhi\n' % (data.box.mins[2], data.box.maxs[2]))

        if masses:
            f.write('\n')
            f.write('Masses\n')
            f.write('\n')
            for i, value in sorted(data.mass_dict.items()):
                if i in data.type_dict.keys():
                    f.write("% 4d % 8.4f # %s\n" % (i, value, data.type_dict[i]))
                else:
                    f.write("% 4d % 8.4f\n" % (i, value))

        # write atoms
        f.write('\n')
        f.write('Atoms\n')
        f.write('\n')
        for i, coord in enumerate(data.pos):

            # style default
            if style == 'default':

                # check for presence of molid
                if numpy.sum(data.molids) > 0:
                    resid = data.molids[i]
                else:
                    resid = 1

                # write default
                f.write('% 6d % 6d % 6d % 6.8f % 8.3f % 8.3f % 8.3f'
                        % (data.indices[i],
                           resid,
                           data.types[i],
                           data.charges[i],
                           coord[0],
                           coord[1],
                           coord[2]))

            # reax style without molid
            elif style == 'reax':

                f.write('% 6d % 6d % 6.8f % 8.3f % 8.3f % 8.3f'
                        % (data.indices[i],
                           data.types[i],
                           data.charges[i],
                           coord[0],
                           coord[1],
                           coord[2]))

            # check if comments are available
            if data.atoms_comments[i]:
                f.write(' # %s' % (data.atoms_comments[i]))

            # make newline
            f.write('\n')

        if vels:
            if data.vels.size:
                f.write('\n')
                f.write('Velocities\n')
                f.write('\n')
                for i, coord in enumerate(data.vels):
                    f.write('% 6d % 8.5f % 8.5f % 8.5f'
                            % (i + 1,
                               coord[0],
                               coord[1],
                               coord[2]))
                    # TODO had to install try catch for some reason
                    try:
                        if data.vels_comments[i]:
                            f.write(' # %s' % (data.vels_comments[i]))
                    except:
                        pass
                    f.write('\n')

        if style == 'default':
            if data.n_bonds() > 0:
                f.write('\n')
                f.write('Bonds\n')
                f.write('\n')
                for i, bond in enumerate(data.bonds):
                    f.write('% 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, bond))[0]),
                               int(list(map(str, bond))[1]),
                               int(list(map(str, bond))[2])))

                    # check for comments
                    if data.bonds_comments[i]:
                        f.write(' # %s' % (data.bonds_comments[i]))
                    f.write('\n')

            if data.n_angles() > 0:
                f.write('\n')
                f.write('Angles\n')
                f.write('\n')
                for i, angle in enumerate(data.angles):
                    f.write('% 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, angle))[0]),
                               int(list(map(str, angle))[1]),
                               int(list(map(str, angle))[2]),
                               int(list(map(str, angle))[3])))

                    if data.angles_comments[i]:
                        f.write(' # %s' % (data.angles_comments[i]))
                    f.write('\n')

            if data.n_dihedrals() > 0:
                f.write('\n')
                f.write('Dihedrals\n')
                f.write('\n')
                for i, dihedral in enumerate(data.dihedrals):
                    f.write('% 6d % 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, dihedral))[0]),
                               int(list(map(str, dihedral))[1]),
                               int(list(map(str, dihedral))[2]),
                               int(list(map(str, dihedral))[3]),
                               int(list(map(str, dihedral))[4])))

                    if data.dihedrals_comments[i]:
                        f.write(' # %s' % (data.dihedrals_comments[i]))
                    f.write('\n')

            if data.n_impropers() > 0:
                f.write('\n')
                f.write('Impropers\n')
                f.write('\n')
                for i, improper in enumerate(data.impropers):
                    f.write('% 6d % 6d % 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, improper))[0]),
                               int(list(map(str, improper))[1]),
                               int(list(map(str, improper))[2]),
                               int(list(map(str, improper))[3]),
                               int(list(map(str, improper))[4]),
                               int(list(map(str, improper))[5])))

                    if data.impropers_comments[i]:
                        f.write(' # %s' % (data.impropers_comments[i]))
                    f.write('\n')

    print("Wrote file '" + filename + "'")


def read_h2o(filename):

    # TODO: improve robustness of xlo regex
    directives = re.compile(r"""
        ((?P<n_atoms>\s*\d+\s+atoms)
        |
        (?P<n_bonds>\s*\d+\s+bonds)
        |
        (?P<n_angles>\s*\d+\s+angles)
        |
        (?P<Coords>\s*Coords)
        |
        (?P<Types>\s*Types)
        |
        (?P<Charges>\s*Charges)
        |
        (?P<Bonds>\s*Bonds)
        |
        (?P<Angles>\s*Angles))
        """, re.VERBOSE)

    data = System(name='H2O')
    data.mass_dict = {1:15.9994, 2:1.0080}
    data.type_dict = {1:'O', 2:'H'}

    # Read file into lines
    print("Reading '" + filename + "'")
    with open(filename, 'r') as f:
        data_lines = f.readlines()

    # iterate through every line
    i = 0
    while i < len(data_lines):
        match = directives.match(data_lines[i])
        if match:
            # initiate the class
            if match.group('n_atoms'):
                fields = data_lines.pop(i).split()
                data.update(n_atoms=int(fields[0]))

            elif match.group('n_bonds'):
                fields = data_lines.pop(i).split()
                data.update(n_bonds=int(fields[0]))

            elif match.group('n_angles'):
                fields = data_lines.pop(i).split()
                data.update(n_angles=int(fields[0]))

            elif match.group('Coords'):

                # pop 'Atoms'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()

                    a_id = int(fields[0])

                    data.indices[a_id - 1] = a_id
                    data.molids[a_id - 1] = 1
                    data.pos[a_id - 1] = numpy.array([float(fields[1]), float(fields[2]), float(fields[3])])


            elif match.group('Types'):

                # pop 'Velocities'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()

                    data.types[int(fields[0]) - 1] = int(fields[1])

            elif match.group('Charges'):

                # pop 'Velocities'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()

                    data.charges[int(fields[0]) - 1] = float(fields[1])


            elif match.group('Bonds'):

                # pop 'Bonds'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.bonds[int(fields[0]) - 1] = list(map(int, fields[1:4]))


            elif match.group('Angles'):

                # pop 'Angles'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.angles[int(fields[0]) - 1] = list(map(int, fields[1:5]))

            else:
                i += 1
        else:
            i += 1

    return data


def write_forcefield_data(filename, inputs):

    f = open(filename, 'w+')

    f.write('# Force Field file written by PyMD\n')
    f.write('\n')
    f.write('\n')
    f.write('pair_style %s' % inputs['pair_style'])
    f.write('\n')
    f.write('pair_modify mix arithmetic tail yes # ATC + Lorentz-Berthelot\n')
    f.write('\n')
    f.write('kspace_style %s ' % inputs['kspace_style'])
    f.write('\n')
    f.write('bond_style %s\n' % inputs['bond_style'])
    f.write('angle_style %s\n' % inputs['angle_style'])
    f.write('dihedral_style %s\n' % inputs['dihedral_style'])
    f.write('improper_style %s\n' % inputs['improper_style'])
    f.write('\n')
    f.write('special_bonds %s\n' % inputs['special_bonds'])
    f.write('\n')
    f.write('# Masses\n')
    f.write('\n')
    for type in inputs['mass'].keys():
        f.write('mass %s %s\n' % (type, inputs['mass'][type]))
    f.write('\n')
    f.write('# Pair Coeffs\n')
    f.write('# \n')
    for type in inputs['pair_coeff'].keys():
        f.write('pair_coeff %s %s\n' % (type, inputs['pair_coeff'][type]))
    f.write('\n')
    f.write('# Bond Coeffs\n')
    f.write('# \n')
    for type in inputs['bond_coeff'].keys():
        f.write('bond_coeff %s %s\n' % (type, inputs['bond_coeff'][type]))
    f.write('\n')
    f.write('# Angle Coeffs\n')
    f.write('# \n')
    for type in inputs['angle_coeff'].keys():
        f.write('angle_coeff %s %s\n' % (type, inputs['angle_coeff'][type]))
    f.write('\n')
    f.write('# Charges of the atoms\n')
    f.write('\n')
    for type in inputs['charge'].keys():
        f.write('set type %s charge %s\n' % (type, inputs['charge'][type]))
    f.write('\n')
    f.close()

def read_frame_lammpstrj(trj, read_velocities=False, read_forces=False):

    system = System()

    # Initialize variables
    box_dims = numpy.empty(shape=(3, 2))

    # --- begin header ---
    trj.readline()  # text "ITEM: TIMESTEP"
    step = int(trj.readline())  # timestep
    trj.readline()  # text "ITEM: NUMBER OF ATOMS"
    n_atoms = int(trj.readline())  # num atoms

    system.update(n_atoms=n_atoms)

    trj.readline()  # text "ITEM: BOX BOUNDS pp pp pp"
    box_dims[0] = trj.readline().split()  # x-dim of box
    box_dims[1] = trj.readline().split()  # y-dim of box
    box_dims[2] = trj.readline().split()  # z-dim of box

    system.box.mins = box_dims[:, 0]
    system.box.maxs = box_dims[:, 1]
    system.box.lengths = system.box.maxs - system.box.mins

    # assign columns
    header = trj.readline()  # text

    type_col = []
    pos_col = []
    vel_col = []
    force_col = []
    charge_col = []

    for i, item in enumerate(header.split(' ')):

        if item.find('type') == 0:
            type_col.append(i - 2)
        elif item.find('q') == 0:
            charge_col.append(i-2)
        elif item.find('x') == 0 or item.find('y') == 0 or item.find('z') == 0:
            pos_col.append(i-2)
        elif item.find('vx') == 0 or item.find('vy') == 0 or item.find('vz') == 0:
            vel_col.append(i-2)
        elif item.find('fx') == 0 or item.find('fy') == 0 or item.find('fz') == 0:
            force_col.append(i-2)


    # --- end header ---

    # --- begin body ---
    for i in range(n_atoms):
        fields = trj.readline().split()
        a_ID = int(fields[0])  # atom ID
        system.indices[a_ID - 1] = a_ID

        if type_col:
            system.types[a_ID - 1] = int(fields[type_col[0]])

        if charge_col:
            system.charges[a_ID - 1] = float(fields[charge_col[0]])

        if pos_col:
            system.pos[a_ID - 1, :] = [float(x) for x in [fields[j] for j in pos_col]]  # coordinates

        if read_velocities and vel_col:
            system.vels[a_ID - 1] = [float(x) for x in [fields[j] for j in vel_col]]  # velocities

        if read_forces and force_col:
            system.forces[a_ID - 1] = [float(x) for x in [fields[j] for j in force_col]]  # forces

    # --- end body ---

    return step, system



def read_lammpstrj(filename, read_velocities=False, read_forces=False, max_size=2, nequi=0):

    trajectory = {}

    statinfo = os.stat(filename)
    file_size = statinfo.st_size/1e9
    memory_available = psutil.virtual_memory().available/1e9
    memory_total = psutil.virtual_memory().total / 1e9
    print('File size is %.1f Gbytes.' % file_size)
    if file_size > max_size or file_size > memory_total*0.8:
        print('File size exceeds %.1f Gbytes.' % max_size)
        print('Total memory available: %.1f' % memory_available)
        print('Current memory available: %.1f' % memory_available)
        print('Allowing for a maximum of 80\% memory usage')
        print('Usage of function is not safe.')
        print('Adjust file size with max_size= in Gbytes')
        print('Not returning anything.')
        return trajectory

    READING = True
    frame = 0
    # loop through trj frames
    with open(filename, 'r') as f:

        # read data
        while READING:
            try:
                step, system = read_frame_lammpstrj(f, read_velocities=read_velocities, read_forces=read_forces)
                system.name = 'timestep %s' % step
                if frame >= nequi or frame == 0:
                    trajectory[step] = system
                frame += 1
                print('Reading timestep/frame %10d/%4i\r' % (step, frame), end='')
            except:
                READING = False

    print('')


    return trajectory

# ------------------------------------
# Experimental taken from asmbox


# function below is untested and is currently no supported. It is available as skeleton for the future
# def read_gro(file_name):
#     """
#     """
#     if not file_name.endswith('.gro'):
#         warnings.warn("File name passed to read_gro() does not end with '.gro'")
#
#     with open(file_name, 'r') as f:
#         sys_name = f.readline().strip()
#         n_atoms = int(f.readline())
#
#         resids = numpy.empty(shape=(n_atoms), dtype='u4')
#         resnames = numpy.empty(shape=(n_atoms), dtype='a5')
#         types = numpy.empty(shape=(n_atoms), dtype='a5')
#         xyz = numpy.empty(shape=(n_atoms, 3), dtype='f2')
#         vel = numpy.empty(shape=(n_atoms, 3), dtype='f2')
#         for i in range(n_atoms):
#             line = f.readline()
#             resids[i] = int(line[:5])
#             resnames[i] = line[5:10].strip()
#             types[i] = line[10:15].strip()
#             xyz[i, 0] = float(line[20:28])
#             xyz[i, 1] = float(line[28:36])
#             xyz[i, 2] = float(line[36:44])
#             try:
#                 vel[i, 0] = float(line[44:52])
#                 vel[i, 1] = float(line[52:60])
#                 vel[i, 2] = float(line[60:68])
#             except:
#                 vel[i, 0] = 0.0
#                 vel[i, 1] = 0.0
#                 vel[i, 2] = 0.0
#
#         box = numpy.zeros(shape=(3, 2))
#         line = list(map(float, f.readline().split()))
#         if len(line) == 3:
#             box[0, 1] = line[0]
#             box[1, 1] = line[1]
#             box[2, 1] = line[2]
#
#     print("Read file '" + file_name + "'")
#     return resids, resnames, types, xyz, vel, box




def write_gro(filename, system, sys_name='GROMACS DATA FILE'):
    """Write gbb to GROMACS .gro file

    Note: to my knowledge GROMACS only deals with positive coordinate
    values. Use method 'positive_coords' to shift coordinates appropriately

    Args:
        grofile (str): name of .gro structure file
    """

    system.box.mins = numpy.zeros(3)
    system.box.maxs = system.box.lengths
    system.wrap()

    with open(filename, 'w') as f:
        f.write(sys_name + '\n')
        f.write(str(system.pos.shape[0]) + '\n')
        for i, coord in enumerate(system.pos):
            if any(system.atoms_comments):
                f.write('%5d%-4s%6s%5d%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f\n'
                          %(system.molids[i],
                            system.atoms_comments[i],
                            system.types[i],
                            i+1,
                            coord[0],
                            coord[1],
                            coord[2],
                            system.vels[i, 0],
                            system.vels[i, 1],
                            system.vels[i, 2]))
            else:
                f.write('%5d%-4s%6s%5d%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f\n'
                        % (system.molids[i],
                           '',
                           system.types[i],
                           i + 1,
                           coord[0],
                           coord[1],
                           coord[2],
                           system.vels[i, 0],
                           system.vels[i, 1],
                           system.vels[i, 2]))
        f.write('%10.5f%10.5f%10.5f\n'
               %(system.box.lengths[0],
                 system.box.lengths[1],
                 system.box.lengths[2]))
    print("Wrote file '" + filename + "'")


def write_top(system, filename):
    """Write forcefield information to GROMACS .top file

    *** THIS IS ONLY THE SKELETON FOR THE FORMAT ***

    TODO:
        -fill in body
    """
    with open(filename, 'w') as f:

        # header
        f.write(';\n')
        f.write('; Topology file \n')
        f.write(';\n')

        # defaults
        f.write('[ defaults ]\n')
        f.write('%d%16d%18s%20.4f%8.4f\n'
                % (1, 3, 'yes', 1, 1)) #  TODO: options need global storage

        # atomtypes
        f.write('[ atomtypes ]\n')
        f.write(';%5s%9s%9s%9s%9s%9s%9s\n'
                % ('nr', 'type', 'resnr', 'residu', 'atom', 'cgnr', 'charge'))

        for i, type in enumerate(system.atomTypes()):
            f.write('%5d%9s%9d%9s%9s%9d%3.5f'
                    % (type, system.type_dict[type], ))

        # moleculetype
        for a in blah:  # BROKED
            f.write('[ moleculetype ]\n')
            f.write('\n')
            f.write('[ atoms ]\n')
            f.write('\n')

            f.write('[ bonds ]\n')
            for bond in system.bonds:
                r = system.bond_types[bond][1]
                k = system.bond_types[bond][2]
                f.write('%6d%7d%4d%18.8e%18.8e\n'
                        %(bond[2], bond[3], 1, r, k))

            f.write('[ angles ]\n')
            f.write('\n')
            f.write('[ dihedrals ]\n')
            f.write('\n')

        # system
        f.write('[ system ]\n')

        # molecules
        f.write('[ molecules ]\n')
    print("Wrote file '" + filename + "'")



def read_log(filename):
    file = open(filename, 'r')
    header_len = 0
    reader = 0
    for k, line in enumerate(file):
        if reader == 1:
            try:
                tmp = [float(x) for x in line.split()]
                if len(tmp) == len(header):
                    data.append(tmp)
            except:
                reader = 0
        if line.find('Step') == 0:
            print('Reading ' + line)
            header = line.split()
            if len(header) != header_len:
                data = []
            reader = 1
            header_len = len(header)
    file.close()

    return header, numpy.asarray(data)