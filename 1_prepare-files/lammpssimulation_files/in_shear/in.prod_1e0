###################################################

units real
dimension 3
newton on
boundary p p f
atom_style full

#-----------------INPUT-----------------#
read_data in.lammpsdata
include in.forcefield

#################### SET SURFACE CHARGE ####################

variable numSC loop 16        # Number of deprotonated sites due to surface charge
variable qHd equal 0
variable qOd equal -0.9
variable qSid equal 0.725
variable Hsc file inputs/H_surfacecharge.dat	# files with lists of atom IDs
variable Osc file inputs/O_surfacecharge.dat
variable Sisc file inputs/Si_surfacecharge.dat
#LOOP
label loop1
set atom ${Hsc} charge ${qHd}	# set atom charges to deprotonated
set atom ${Osc} charge ${qOd}
set atom ${Sisc} charge ${qSid}
next numSC			# take next atoms in list and next deprotonated site number
next Hsc Osc Sisc
jump in.simulation loop1	#jump back up to start loop again

#################### settings ####################

reset_timestep 0 # set timestep counter to x

variable Nprod equal 40000000 		# 10 ns
variable transittime equal 500 

variable reacttime equal 67150500

variable Nthermo equal 1000    	# every 1 ps give themodata
variable Ndump equal 200

#---------------------------------------#

variable Temp equal 298       # Temperature in K
variable Pres equal 1.0       # Pressure in atm.
variable tstep equal 2.0      # 1fs

run_style verlet

neighbor 2.0 bin
neigh_modify every 1 delay 0 check yes 	# rebuilt list every step, do not delay and rebuilt if an atom has moved half the skin distance or more

thermo_style one
thermo ${Nthermo}

dump VMD all dcd ${Ndump} out.dcd 	# every 1 ps dump trj
dump_modify VMD unwrap yes

#################### INITIALIZATION ####################

print ""
print "FIXES"
print ""

timestep ${tstep}

variable Lz equal (zhi-8)

region BLOCK1 block -1000 1000 -1000 1000 -1000 $(-v_Lz)
region BLOCK2 block -1000 1000 -1000 1000 $(v_Lz) 1000

group REGION1 region BLOCK1
group REGION2 region BLOCK2

group RIGID type 3 4
group RIGID1 intersect RIGID REGION1
group RIGID2 intersect RIGID REGION2

# ################ NORMAL ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 1 2 3 4 5 6 7 8 9 10 11 12 13
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
################ VISCOSITY ####################

group SHAKE type 1 2 8 11
group ALL type 1 2 3 4 5 6 7 8 9 10 11 12 13
group SIM subtract ALL RIGID1 RIGID2
group FLUID type 1 2 12 13

fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)

fix move1 RIGID1 move linear 0.0002 0 0
fix move2 RIGID2 move linear -0.0002 0 0

compute f1 RIGID1 reduce sum fx
compute f2 RIGID2 reduce sum fx

fix write all ave/time 1 ${Ndump} ${Ndump} c_f1 c_f2 file out.forces

# ################ EMOSIS ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 3 4 5 6 7 8 9 10 11
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
# fix 2 FLUID nve
# fix kick FLUID efield 0.02 0.0 0.0
#
# ################ Poiseuille ####################
# 
# group SHAKE type 1 2 8 11
# group ALL type 3 4 5 6 7 8 9 10 11
# group SIM subtract ALL RIGID1 RIGID2
# group FLUID type 1 2 12 13
# 
# fix 0 SHAKE shake 1.0e-4 1000 0 b 1 3 a 1
# fix 1 SIM nvt temp ${Temp} ${Temp} $(100*dt)
# 
# fix 2 FLUID nve
# fix kick FLUID gravity 0.00005155942 vector 1 0 0 # equivalent to 75 atm in our system

################ SIMULATION ####################

timestep ${tstep}
run ${Nprod}

write_data out.lammpsdata nocoeff

quit

