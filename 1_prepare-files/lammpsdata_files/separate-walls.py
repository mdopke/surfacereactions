#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water
from PyMD.System import System, Box
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--style', type=str)
    parser.add_argument('--wrap', type=str, help='True/False', default='True')
    parser.add_argument('--L', type=float, help='Always separates along the z axis')

    args = parser.parse_args()

    if args.style == None:
        args.style = 'default'

    # read file
    system = read_lammpsdata(args.fin, style=args.style)

    # separate walls
    system.pos[numpy.where(system.pos[:, 2] < 0)[0], 2] -= args.L/2
    system.pos[numpy.where(system.pos[:, 2] > 0)[0], 2] += args.L/2

    # expand pbc
    system.box.lengths[2] += args.L
    system.box.mins[2] -= args.L/2
    system.box.maxs[2] += args.L/2

    if args.wrap == 'True':
        system.wrap()

    # settings to start with
    # atoms = {1: 'Ow', 2: 'Hw'}
    # bonds = {1: 'OwHw'}
    # angles = {1: 'HwOwHw'}

    write_lammpsdata(args.fout, system, style=args.style)
