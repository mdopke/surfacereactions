#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water
from PyMD.System import System, Box
import argparse
from random import randint, seed

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--charge', type=str)
    parser.add_argument('--seed', type=str)
    parser.add_argument('--style', type=str)

    args = parser.parse_args()

    if args.style == None:
        args.style = 'default'
        
#%% uncomment for parser arguments
    system = read_lammpsdata(args.fin, style=args.style)    # read file
    C = int(args.charge)   
    randseed = args.seed
    
#%% uncomment for manual input arguments        
    # system = read_lammpsdata("outputs\\in.lammpsdata", style='default')
    # C = -100 # charge density mC/m^2
    # randseed = 1238


#%% setting up the surface charge    
    area = system.box.lengths[0]*system.box.lengths[1]      
    # area of one wall in A^2 
    area_convertion = 10**(-20)                             
    # (1 m^-2 = 10^-20 A^-2)
    electron_convertion = 6.24150975*10**18                 
    # (1 C = 6.24150975*10^18 e)
    charge = C*10**(-3)*area_convertion*electron_convertion # in e/A^2 
    n_deprotonated = round(-charge*area)                    
    # rounded number of deprotonated sites per wall
    
    print(int(n_deprotonated), "deprototonated sites per wall needed")
    if -charge*area % 1 < 0.5:
        print("Number of deprotonated sites rounded down, surface charge is less negative than", C, "mC/m^2, pH is lower")
    elif -charge*area % 1 > 0.5:
        print("Number of deprotonated sites rounded up, surface charge is more negative than", C, "mC/m^2, pH is higher")   
    left = numpy.where(system.pos[:, 2] < 0)[0]+1   # all left atoms
    right = numpy.where(system.pos[:, 2] > 0)[0]+1  # all right atoms
    Hs = numpy.where(system.types == 11)[0]+1    # all atoms of type 11=Hs

    bonds_OH = numpy.where(system.bonds[:,0] == 3)[0] 
    # indices of all silanol O-H bonds = type 3
    bonds_SiO = numpy.where(system.bonds[:,0] == 2)[0] 
    # indices of all silanol Si-O bonds = type 2

    SiOH = []
    SiOH_left = []
    SiOH_right = []
    
    for i in bonds_OH:
        a = system.bonds[i,1]   # first atom
        b = system.bonds[i,2]   # second atom
        if a in Hs:
            H_i = a
            O_i = b
        elif b in Hs:
            H_i = b
            O_i = a
        else:
            print("ERROR: Atom not found in list")    
        for j in bonds_SiO:
                c = system.bonds[j,1]   # first atom
                d = system.bonds[j,2]   # second atom
                if O_i == c:      # O is first atom
                    Si_i = d
                elif O_i == d:    # O is second atom
                    Si_i = c
        SiOH.append([Si_i, O_i, H_i])
    SiOH = numpy.asarray(SiOH)

    for i in range(len(SiOH)):
        if SiOH[i,0] in left:   # Si atom in left set
            SiOH_left.append(SiOH[i,:])
        elif SiOH[i,0] in right:   # Si atom in right set
            SiOH_right.append(SiOH[i,:])
        else:
            print("ERROR: Atom not found in left or right set")     
    
    SiO_left = []
    SiO_right = []
    #%%
    seed(int(randseed))
    for s in range(int(n_deprotonated)):
        index = randint(0, len(SiOH_left)-1)    # choose random left silanol
        ID = SiOH_left.pop(index)               # delete from list
        SiO_left.append(ID)                     # add to deprotonated list
        index = randint(0, len(SiOH_right)-1)   
        ID = SiOH_right.pop(index)
        SiO_right.append(ID)
#%% no double dangling silanols (for simplicity)
    SiO_left = numpy.asarray(SiO_left)
    SiO_right = numpy.asarray(SiO_right)
    
    Siss = numpy.where(system.types == 10)[0]+1 # all atoms of type 10=Siss
    Siss = numpy.c_[Siss,numpy.zeros(len(Siss),dtype='>i4')]
    
    for i in range(len(Siss)):
        if Siss[i,0] in SiO_right[:,0]:
            Siss[i,1] -= 1
        elif Siss[i,0] in SiO_left[:,0]:
            Siss[i,1] -= 1
    for i in Siss[:,1]:
        if i < -1:
            print("ERROR: two silanols of a geminal are deprotonated in surface charge input file, try different random seed")
            
#%% writing output files
    file_Si = open("outputs/Si_surfacecharge.dat","w")
    file_O = open("outputs/O_surfacecharge.dat","w")
    file_H = open("outputs/H_surfacecharge.dat","w")
    for i in range(len(SiO_left)):
        Si_sc = numpy.asarray(SiO_left[i])[0]
        O_sc = numpy.asarray(SiO_left[i])[1]
        H_sc = numpy.asarray(SiO_left[i])[2]
        file_Si.write("{}\n".format(Si_sc))
        file_O.write("{}\n".format(O_sc))
        file_H.write("{}\n".format(H_sc))
    for i in range(len(SiO_right)):
        Si_sc = numpy.asarray(SiO_right[i])[0]
        O_sc = numpy.asarray(SiO_right[i])[1]
        H_sc = numpy.asarray(SiO_right[i])[2]
        file_Si.write("{}\n".format(Si_sc))
        file_O.write("{}\n".format(O_sc))
        file_H.write("{}\n".format(H_sc))
    print("Surface charge initialization data files written")
    file_Si.close()
    file_O.close()
    file_H.close()
    
    
    
    
    
    
    
    
