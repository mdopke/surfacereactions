------------LAMMPSDATA------------
init.lammpsdata is data file of two sparated walls with water, NO IONS
To add ions:
	python ./PyMD/TOOLS/write_LAMMPSDATA_ions.py --fin init.lammpsdata --fout in.lammpsdata --box 35 35 40 --ion symb:Na type:12 add:34 --ion symb:Cl type:13 add:18
in.lammpsdata has water,34 Na and 18 Cl

------------FORCEFIELD------------
init.forcefield is file made by force-fields.py including Na and Cl parameters.
in.forcefield has epsilon of Hs manually set to zero.

------------SURFACE CHARGE------------
surface-charge.py makes 3 files of Si, O and H atom numbers that should be deprotonated initially.
	inputs: --fin, --charge, --seed, (--style)
	example: python surface-charge.py --fin outputs\\in.lammpsdata --charge -100 --seed 1234

