#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water
from PyMD.System import System, Box
import argparse
from random import randint, seed

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--deprotonatedOfile', type=str)     # text file with atom numbers of deprotonated O
    parser.add_argument('--seed', type=str)
    parser.add_argument('--steps', type=str)
    parser.add_argument('--k_iso', type=str)    # optional
    parser.add_argument('--k_gem', type=str)    # optional
    parser.add_argument('--k_vic', type=str)    # optional
    parser.add_argument('--style', type=str)    # optional

    args = parser.parse_args()

    if args.style == None:
        args.style = 'default'
    if args.k_iso == None:
        args.k_iso = 1
    if args.k_gem == None:
        args.k_gem = 1
    if args.k_vic == None:
        args.k_vic = 1
        
#%% uncomment for parser arguments
    system = read_lammpsdata(args.fin, style=args.style)    # read file
    file = open(args.deprotonatedOfile,"r")
    randseed = args.seed
    steps = args.steps
    k_iso = args.k_iso # ratio in integers
    k_gem = args.k_gem
    k_vic = args.k_vic 
    
#%% uncomment for manual input arguments        
    # system = read_lammpsdata("W:\\1235\Initialization\outputs\min.lammpsdata", style='default')
    # file = open(r"W:\\1235\Initialization\inputs\O_surfacecharge.dat","r")
    # #file = open("outputs\\O_1238_50.dat","r") 
    # randseed = 1238
    # steps = 20000
    # k_iso = 1 # ratio in integers
    # k_gem = 1
    # k_vic = 1
    
#%% make SiOH array
    left = numpy.where(system.pos[:, 2] < 0)[0]+1   # all left atoms
    right = numpy.where(system.pos[:, 2] > 0)[0]+1  # all right atoms
    Hs = numpy.where(system.types == 11)[0]+1    # all atoms of type 11=Hs
    Os = numpy.where(system.types == 8)[0]+1    # all atoms of type 8=Os
    Sib = numpy.where(system.types == 4)[0]+1    # all atoms of type 4=Sib
    Si_sil = numpy.append((numpy.where(system.types == 9)[0]+1),(numpy.where(system.types == 10)[0]+1)) # all atom numbers of Si atoms that are part of a silanol
    
    bonds_OH = numpy.where(system.bonds[:,0] == 3)[0] # indices of all silanol O-H bonds = type 3
    bonds_SiO = numpy.where(system.bonds[:,0] == 2)[0] # indices of all silanol Si-O bonds = type 2

    SiOH = []
    for i in bonds_OH:
        a = system.bonds[i,1]   # first atom
        b = system.bonds[i,2]   # second atom
        if a in Hs:
            H_i = a
            O_i = b
        elif b in Hs:
            H_i = b
            O_i = a
        else:
            print("ERROR: Atom not found in list")    
        for j in bonds_SiO:
                c = system.bonds[j,1]   # first atom
                d = system.bonds[j,2]   # second atom
                if O_i == c:      # O is first atom
                    Si_i = d
                elif O_i == d:    # O is second atom
                    Si_i = c
        SiOH.append([Si_i, O_i, H_i, 0]) # add column for probability
    SiOH = numpy.asarray(SiOH)

#%% assign isolated/geminal/vicinal status
    Siss = numpy.where(system.types == 10)[0]+1 # all atoms of type 10=Siss (geminal)
    for i in range(len(SiOH)):
        #print("Si atom number",SiOH[i,0])
        if SiOH[i,0] in Siss:   # if Si is part of geminal
            SiOH[i,3] = k_gem
            #print("geminal")
        elif SiOH[i,0] not in Siss:
            bonds = numpy.append(numpy.where(system.bonds[:,1] == SiOH[i,0])[0],numpy.where(system.bonds[:,2] == SiOH[i,0])[0]) # indices of the bonds where the Si atom is present
            #print("bonds=",bonds)
            bridge = []                 # empty list for atom numbers of bridge atoms
            check = []
            for j in bonds:
                a = system.bonds[j,1]   # first atom
                b = system.bonds[j,2]   # second atom
                #print("a=",a,"b=",b)
                if a not in Os and b not in Os: # discard the silanol bond
                    if a == SiOH[i,0]:
                        bridge = numpy.append(bridge, b)    # add atom number of bonded atom
                    elif b == SiOH[i,0]:
                        bridge = numpy.append(bridge, a)    # add atom number of bonded atom
            #print("bridge=",bridge)
            for k in bridge:
                bonds_2 = numpy.append(numpy.where(system.bonds[:,1] == k)[0],numpy.where(system.bonds[:,2] == k)[0]) # indices of the bonds where the brigde oxygen atom is present
                #print("bonds_2=",bonds_2)
                for l in bonds_2:
                    a = system.bonds[l,1]   # first atom
                    b = system.bonds[l,2]   # second atom
                    #print("a=",a,"b=",b)
                    if a != SiOH[i,0] and b != SiOH[i,0]:   # discard original Si 
                        if a == k:
                            check = numpy.append(check,b)   # find atom number to check
                        elif b == k:
                            check = numpy.append(check,a)   # find atom number to check
            #print("check=",check)
            vic = 0
            for m in check:
                if m in Si_sil:
                    vic += 1
            if vic == 0:
                SiOH[i,3] = k_iso
                #print("isolated")
            elif vic != 0:
                SiOH[i,3] = k_vic
                #print("vicinal")
    

#%% make SiO list    
    Od =  file.readlines()      # read deprotonated Os
    file.close()
    Od = [i.split() for i in Od]
    Od = numpy.asarray(Od)

    dep = []
    for i in range(len(Od)):
        s = int(Od[i])
        index = numpy.where(SiOH[:,1] == s)[0]  # index where Od is in SiOH list
        ID = SiOH[index]                        # save that row
        SiOH = numpy.delete(SiOH,index,axis=0)  # delete that row for SiOH list
        dep.append(ID)                          # add to SiO list
    SiO = numpy.zeros([len(dep),4],dtype='>i4') # make empty list for deprotonated SiOH
    for i in range(len(dep)):                   # SiO as numpy array
        SiO[i,:]= dep[i] 
    
      
#%% account for geminal silanols (do not deprotonate both silanols)
    Siss = numpy.where(system.types == 10)[0]+1 # all atoms of type 10=Siss
    Siss = numpy.c_[Siss,numpy.zeros(len(Siss),dtype='>i4')] # add row for charge
    for i in range(len(Siss)):
        if Siss[i,0] in SiO[:,0]:
            Siss[i,1] -= 1

#%% make left and right lists
    SiOH_left = []
    SiOH_right = []
    for i in range(len(SiOH)):
        if SiOH[i,0] in left:           # Si atom in left set
            SiOH_left.append(SiOH[i,:])
        elif SiOH[i,0] in right:        # Si atom in right set
            SiOH_right.append(SiOH[i,:])
        else:
            print("ERROR: Atom not found in left or right set")
    
    SiO_left = []
    SiO_right = []
    for i in range(len(SiO)):
        if SiO[i,0] in left:           # Si atom in left set
            SiO_left.append(SiO[i,:])
        elif SiO[i,0] in right:        # Si atom in right set
            SiO_right.append(SiO[i,:])
        else:
            print("ERROR: Atom not found in left or right set")
  
#%% assign different probability   
    for i in range(len(SiO_left)):  # silanols with higher probability appear more times in the list
        k = SiO_left[i][3]
        for j in range(0,k-1):
            SiO_left.append(SiO_left[i])
    for i in range(len(SiOH_left)):  # silanols with higher probability appear more times in the list
        k = SiOH_left[i][3]
        for j in range(0,k-1):
            SiOH_left.append(SiOH_left[i])
    for i in range(len(SiO_right)):  # silanols with higher probability appear more times in the list
        k = SiO_right[i][3]
        for j in range(0,k-1):
            SiO_right.append(SiO_right[i])
    for i in range(len(SiOH_right)):  # silanols with higher probability appear more times in the list
        k = SiOH_right[i][3]
        for j in range(0,k-1):
            SiOH_right.append(SiOH_right[i])
                      
#%% (de)protonate
    seed(int(randseed))

    deprotonated_left = []
    protonated_left = []
    
    for s in range(int(steps)):  
        indexd = randint(0, len(SiOH_left)-1)          # choose random left SiOH to deprotonate
        if SiOH_left[indexd][0] in Siss[:,0]:
            Siss[numpy.where(Siss[:,0] == SiOH_left[indexd][0])[0],1] -= 1 # reduce charge of Si
            charged = Siss[numpy.where(Siss[:,0] == SiOH_left[indexd][0])[0],1]
        elif SiOH_left[indexd][0] not in Siss[:,0]:
            charged = -1
        else: 
            print("ERROR")
        indexp = randint(0, len(SiO_left)-1)                # choose random left SiO to protonate  
        if SiO_left[indexp][0] in Siss[:,0]:                # if silanol is part of geminal
            Siss[numpy.where(Siss[:,0] == SiO_left[indexp][0])[0],1] += 1     # charge of Si +1         
            chargep = Siss[numpy.where(Siss[:,0] == SiO_left[indexp][0])[0],1]
        elif SiO_left[indexp][0] not in Siss[:,0]:
            chargep = 0
        
        Od_index = SiOH_left[indexd][1] # atom number of deprotonating oxygen
        #print("length=",len(SiOH_left))
        indices = [] # indices of items to be removed
        for i in range(len(SiOH_left)):
            #print("i=",i)
            if SiOH_left[i][1] == Od_index:
                indices.append(i)
        #print("indicesd",indices)
        for j in sorted(indices, reverse=True):
            #print("j=",j)
            IDd = SiOH_left.pop(j) # delete that row from left SiOH
            #print("IDd=",IDd)
            SiO_left.append(IDd)   # append deprotonated row to SiO
              
        Op_index = SiO_left[indexp][1] # atom number of protonating oxygen
        #print("length=",len(SiO_left))
        indices = [] # indices of items to be removed
        for i in range(len(SiO_left)):
            #print("i=",i)
            if SiO_left[i][1] == Op_index:
                indices.append(i)
        #print("indicesp",indices)
        for j in sorted(indices, reverse=True):
            #print("j=",j)
            IDp = SiO_left.pop(j) # delete that row from left SiOH
            #print("IDp=",IDp)
            SiOH_left.append(IDp)   # append deprotonated row to SiO
    
        deprotonated_left.append(numpy.append(IDd,charged)) # add to list of deprotonation events
        protonated_left.append(numpy.append(IDp,chargep))   # add to list of deprotonation events
    
    deprotonated_right = []
    protonated_right = []
    
    for s in range(int(steps)):  
        indexd = randint(0, len(SiOH_right)-1)          # choose random left SiOH to deprotonate
        if SiOH_right[indexd][0] in Siss[:,0]:
            Siss[numpy.where(Siss[:,0] == SiOH_right[indexd][0])[0],1] -= 1 # reduce charge of Si
            charged = Siss[numpy.where(Siss[:,0] == SiOH_right[indexd][0])[0],1]
        elif SiOH_right[indexd][0] not in Siss[:,0]:
            charged = -1
        else: 
            print("ERROR")
        indexp = randint(0, len(SiO_right)-1)                # choose random left SiO to protonate  
        if SiO_right[indexp][0] in Siss[:,0]:                # if silanol is part of geminal
            Siss[numpy.where(Siss[:,0] == SiO_right[indexp][0])[0],1] += 1     # charge of Si +1         
            chargep = Siss[numpy.where(Siss[:,0] == SiO_right[indexp][0])[0],1]
        elif SiO_right[indexp][0] not in Siss[:,0]:
            chargep = 0
            
        Od_index = SiOH_right[indexd][1] # atom number of deprotonating oxygen
        #print("length=",len(SiOH_right))
        indices = [] # indices of items to be removed
        for i in range(len(SiOH_right)):
            #print("i=",i)
            if SiOH_right[i][1] == Od_index:
                indices.append(i)
        #print("indicesd",indices)
        for j in sorted(indices, reverse=True):
            #print("j=",j)
            IDd = SiOH_right.pop(j) # delete that row from right SiOH
            #print("IDd=",IDd)
            SiO_right.append(IDd)   # append deprotonated row to SiO
              
        Op_index = SiO_right[indexp][1] # atom number of protonating oxygen
        #print("length=",len(SiO_right))
        indices = [] # indices of items to be removed
        for i in range(len(SiO_right)):
            #print("i=",i)
            if SiO_right[i][1] == Op_index:
                indices.append(i)
        #print("indicesp",indices)
        for j in sorted(indices, reverse=True):
            #print("j=",j)
            IDp = SiO_right.pop(j) # delete that row from right SiOH
            #print("IDp=",IDp)
            SiOH_right.append(IDp)   # append deprotonated row to SiO

        deprotonated_right.append(numpy.append(IDd,charged)) # add to list of deprotonation events
        protonated_right.append(numpy.append(IDp,chargep))   # add to list of deprotonation events
        
#%% list of unique SiO- oxygens
    oxygen = []
    for i in range(len(SiO_right)):
        oxygen.append(SiO_right[i][1])
    for i in range(len(SiO_left)):
        oxygen.append(SiO_left[i][1])

    output = []
    for x in oxygen:
        if x not in output:
            output.append(x)

#%% write output files
    Sip_left = open("outputs/which_Si_protonates.left","w")
    Op_left = open("outputs/which_O_protonates.left","w")        
    Hp_left = open("outputs/which_H_protonates.left","w")
    Cp_left = open("outputs/charge_of_protonating_Si.left","w")
    Sid_left = open("outputs/which_Si_deprotonates.left","w")
    Od_left = open("outputs/which_O_deprotonates.left","w")        
    Hd_left = open("outputs/which_H_deprotonates.left","w")
    Cd_left = open("outputs/charge_of_deprotonating_Si.left","w")
    
    Sip_right = open("outputs/which_Si_protonates.right","w")   
    Op_right = open("outputs/which_O_protonates.right","w")  
    Hp_right = open("outputs/which_H_protonates.right","w")   
    Cp_right = open("outputs/charge_of_protonating_Si.right","w")
    Sid_right = open("outputs/which_Si_deprotonates.right","w")   
    Od_right = open("outputs/which_O_deprotonates.right","w")  
    Hd_right = open("outputs/which_H_deprotonates.right","w")    
    Cd_right = open("outputs/charge_of_deprotonating_Si.right","w")
    
    for i in range(len(deprotonated_left)):
        charge_deprotonated_left = 0
        charge_deprotonated_right = 0
        charge_protonated_left = 0
        charge_protonated_right = 0
        if deprotonated_left[i][4] == -1:
            charge_deprotonated_left = 0.725000 # set charge of single dangling Si
        elif deprotonated_left[i][4] == -2:
            charge_deprotonated_left = 0.350000 # set charge of double dangling Si
        if deprotonated_right[i][4] == -1:
            charge_deprotonated_right = 0.725000 # set charge of single dangling Si
        elif deprotonated_right[i][4] == -2:
            charge_deprotonated_right = 0.350000 # set charge of double dangling Si
        
        if protonated_left[i][4] == 0:
            charge_protonated_left = 1.100000 # set charge of protonated Si
        elif protonated_left[i][4] == -1:
            charge_protonated_left = 0.725000 # set charge of single dangling Si
        if protonated_right[i][4] == 0:
            charge_protonated_right = 1.100000 # set charge of protonated Si
        elif protonated_right[i][4] == -1:
            charge_protonated_right = 0.725000 # set charge of single dangling Si
            
        Sip_left.write("{}\n".format(protonated_left[i][0]))      # write files with atom numbers of which Si protonates
        Op_left.write("{}\n".format(protonated_left[i][1]))
        Hp_left.write("{}\n".format(protonated_left[i][2]))
        Cp_left.write("{}\n".format(charge_protonated_left))
        Sid_left.write("{}\n".format(deprotonated_left[i][0]))
        Od_left.write("{}\n".format(deprotonated_left[i][1]))
        Hd_left.write("{}\n".format(deprotonated_left[i][2]))
        Cd_left.write("{}\n".format(charge_deprotonated_left))
        
        Sip_right.write("{}\n".format(protonated_right[i][0]))
        Op_right.write("{}\n".format(protonated_right[i][1]))
        Hp_right.write("{}\n".format(protonated_right[i][2]))
        Cp_right.write("{}\n".format(charge_protonated_right))
        Sid_right.write("{}\n".format(deprotonated_right[i][0]))
        Od_right.write("{}\n".format(deprotonated_right[i][1]))
        Hd_right.write("{}\n".format(deprotonated_right[i][2]))
        Cd_right.write("{}\n".format(charge_deprotonated_right))
    
    Sip_left.close()
    Op_left.close()
    Hp_left.close()
    Cp_left.close()
    Sid_left.close()
    Od_left.close()
    Hd_left.close()
    Cd_left.close()
    
    Sip_right.close()
    Op_right.close()
    Hp_right.close()
    Cp_right.close()
    Sid_right.close()
    Od_right.close()
    Hd_right.close()
    Cd_right.close()
    
    oxygen = open("outputs/deprotonated_oxygens_at_end.dat","w")
    for i in range(len(output)):
        oxygen.write("{}\n".format(output[i]))
    oxygen.close()
    
    print("output files written")   

    
    
    
    
    
    
    
        
        
        
