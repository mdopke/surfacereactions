#!/usr/bin/env

import numpy as np
import itertools
import copy


class Indices:
    '''
    Class creted for easy indexing of atoms, requires FF types at the very least
    '''

    def __init__(self,
                 atom_types={'Ow': 1, 'Hw': 2, 'Ob': 3, 'Sib': 4, 'Od':5, 'Sid':6, 'Sidd':7, 'Os':8, 'Sis':9, 'Siss':10, 'Hs':11, 'Cl':12, 'Na':13, 'Ca':14},
                 bond_types={'OwHw': 1, 'OSi': 2, 'OsHs': 3},
                 angle_types={'HwOwHw': 1, 'SiOSi': 2, 'OSiO': 4, 'SiOH': 5}):

        self.atom_types = atom_types
        self.attributes = [pair[1] for pair in sorted((value, key) for key, value in atom_types.items())]
        self.bond_types = bond_types
        self.angle_types = angle_types

        self.indices = {}
        for i, atr in enumerate(self.atom_types):
            self.indices[atr] = []


    def printConfiguration(self):
        '''Print atom configuration'''
        c = 1
        print('Atom\t#\t# of atoms\tmasses\tcharges')
        for i, atr in enumerate(self.atom_types):
            print('%s\t%i\t%i' % (atr, self.atom_types[atr], len(self.indices[atr])))
            c += 1

    def all(self):
        '''Returns all indices'''
        result = []
        for i, atr in enumerate(self.atom_types):
            for j in self.indices[atr]:
                result.append(j)

        return result

    def select(self, atr_):
        '''allows to select multiple indices based on str input'''
        if atr_.istitle():
            result = []
            for i, atr in enumerate(self.atom_types):
                if atr.find(atr_) == 0:
                    for j in self.indices[atr]:
                        result.append(j)
        else:
            result = []
            for i, atr in enumerate(self.atom_types):
                if atr.find(atr_) > 0:
                    for j in self.indices[atr]:
                        result.append(j)

        return result

    def remove(self, j):
        '''Removes an index from arrays'''
        for i, atr in enumerate(self.atom_types):
            if j in self.indices[atr]:
                self.indices[atr].remove(j)

    def clear(self):
        '''Clears all index arrays'''
        for i, atr in enumerate(self.atom_types):
            self.indices[atr] = []

    def charge(self):
        '''Brute force charge'''

        return len(self.select('Si')) * 2.4 - 1.2 * len(self.select('O')) + 0.6 * len(self.select('Od'))

        # total_charge = 0
        # for atr in self.attributes:
        #     total_charge += self.charges[atr] * len(self.indices[atr])
        #
        # # add hydrogens and ions in fake
        # total_charge += len(self.indices['Os']) * 0.4 + len(self.indices['Od'])
        #
        # return total_charge

    def update(self, system):
        system = self.update_types(system)
        system = self.update_indices(system)
        self.clear()
        self.assign_indices(system)
        system = self.assign_bond_types(system)
        system = self.assign_angle_types(system)

        return system


    def assign_indices(self, system):
        '''Assign atom indices'''
        c = 0
        for i, atr in enumerate(self.atom_types):
            for id in list(np.where(system.types == self.atom_types[atr])[0]):
                c += 1
                print('Assigning indices %i\r' % c, end='')
                self.indices[atr].append(id)
        print('')


    def update_types(self, system):
        '''We might have changed atom types through operations, here we update'''
        types = np.zeros(len(system.types))
        for i, atr in enumerate(self.atom_types):
            print('Updating atom types %i\r' % (i + 1), end='')
            types[self.indices[atr]] = self.atom_types[atr]
        system.types = types
        print('')

        return system


    def update_indices(self, system):
        '''Scan through indices and eliminate respective atoms in system'''

        # make hard copies
        system = copy.deepcopy(system)
        atom_indices = copy.deepcopy(self.all())

        # initialize variables
        indices = []
        data = np.zeros((len(system.types), 3)) - 1

        # iterate through system
        c = 0
        for i, j in enumerate(system.types):
            print('Updating indices %i\r' % (i + 1), end='')

            data[i, 0] = i

            if i not in atom_indices:
                # append to list of indices to remove
                indices.append(i)

            elif i in atom_indices:
                # if it stays it has an old index i and gets a new index c
                data[i, 1] = i
                data[i, 2] = c
                c += 1
        print('')

        # eliminate indices that do not exist
        types = [j for i, j in enumerate(system.types) if i not in indices]
        system.types = np.asarray(types)

        # recount number of atoms
        system.n_atoms = len(system.types)

        # now eliminate positions or removed atoms
        xyz = [list(j) for i, j in enumerate(system.xyz) if i not in indices]
        system.xyz = np.asarray(xyz)

        # eliminate charges of removed atoms
        charges = [j for i, j in enumerate(system.charges) if i not in indices]
        system.charges = np.asarray(charges)

        # eliminate resids of removed atoms
        try:
            resids = [j for i, j in enumerate(system.resids) if i not in indices]
            system.resids = np.asarray(resids)
        except:
            pass

        # remove bonds that have no index in atoms
        indices = []
        for i, j in enumerate(system.bonds):
            if any(k - 1 not in data[:, 1] for k in j[1:]):
                indices.append(i)

        bonds = [list(j) for i, j in enumerate(system.bonds) if i not in indices]
        bonds = np.asarray(bonds)

        tmp = copy.deepcopy(bonds)

        for i in range(len(data)):
            bonds[np.where(tmp[:, 1] == data[i, 1] + 1)[0], 1] = data[i, 2] + 1
            bonds[np.where(tmp[:, 2] == data[i, 1] + 1)[0], 2] = data[i, 2] + 1

        system.bonds = bonds
        system.n_bonds = len(bonds)


        # we do not remove angles because we create them new from bonds everytime

        # # remove angles that have no index in atoms
        # indices = []
        # for i, j in enumerate(system.angles):
        #     if any(k - 1 not in data[:, 1] for k in j[1:]):
        #         indices.append(i)
        #
        # angles = [list(j) for i, j in enumerate(system.angles) if i not in indices]
        # angles = np.asarray(angles)
        #
        # tmp = copy.deepcopy(angles)
        #
        # for i in range(len(data)):
        #     angles[np.where(tmp[:, 1] == data[i, 1] + 1)[0], 1] = data[i, 2] + 1
        #     angles[np.where(tmp[:, 2] == data[i, 1] + 1)[0], 2] = data[i, 2] + 1
        #     angles[np.where(tmp[:, 3] == data[i, 1] + 1)[0], 3] = data[i, 2] + 1
        #
        # system.angles = angles
        # system.n_angles = len(angles)

        return system


    def assign_bond_types(self, system):
        '''Assign bonds'''

        # go through possible bond types
        for i, atr in enumerate(self.bond_types):
            print('Assigning bond types %s %i\r' % (atr, 0), end='')

            cut = split(atr)
            atom1 = atr[:cut[0]]
            atom2 = atr[cut[0]:]

            # if atr == 'OSi':
            #     print('')
            #     print(atr, self.select(atom1), self.select(atom2))
            #     print('')
            # go through bonds array

            c = 0
            for i2, j2 in enumerate(system.bonds):
                # print(j2)
                # go through both bond atoms
                for k2 in [[1, 2], [2, 1]]:
                    # print(j2[k2[0]] - 1 in self.select(atom1), j2[k2[1]] - 1 in self.select(atom2))
                    # define what type atoms are
                    if j2[k2[0]] - 1 in self.select(atom1) and j2[k2[1]] - 1 in self.select(atom2):
                        # assign type
                        c += 1
                        print('Assigning bond types %s %i\r' % (atr, c), end='')
                        system.bonds[i2, 0] = self.bond_types[atr]
            print('')
        # adjust number of bonds
        system.n_bonds = len(system.bonds)

        return system

    def assign_angle_types(self, system):
        '''assign angles'''

        def create_angles_from_bonds(bonds, indices1, indices2, indices3, angle_type):
            '''
            Function that computes all angle combinations between existing bonds.

            From its implementation it should work to just add it at the end
            It is entirely bond array existance dependent.
            When creating vacuum the  bonds will be updated which should automatically correct angles
            If using this function

            need to specify all angles Si - O - Si, O - Si - O ... separately

            :param bonds:
            :param indices1:
            :param indices2:
            :param indices3:
            :param type:
            :return:
            '''

            indices1 = copy.deepcopy(indices1)
            indices2 = copy.deepcopy(indices2)
            indices3 = copy.deepcopy(indices3)

            # start counter
            c = 0

            # start angle list
            angles = []

            # maybe distinguish in the future between BO-Si-BO and BO-Si-NBO

            # iterate through center atom and add the edges
            for i in indices2:  # Si-O-Si and Si-O-Hbonds

                # make temporary list
                a = []

                # iterate through array of bonds
                for j in bonds:

                    # find bond partners
                    for k in [[1, 2], [2, 1]]:
                        if i + 1 == j[k[0]]:
                            a.append(j[k[1]])

                # make possible combinations with described indices
                for k in itertools.combinations(a, 2):

                    # only if both bond pairs are in indices as described add to angles
                    if (k[0] - 1 in indices1 and k[1] - 1 in indices3) \
                            or (k[0] - 1 in indices3 and k[1] - 1 in indices1):
                        angles.append([angle_type, k[0], i + 1, k[1]])

                        c += 1
                        print("Creating angles: %d \r" % c, end="")
            print('')
            if angles == []:
                angles = np.empty(shape=(0, 4), dtype=int)

            return np.asarray(angles).astype(int)

        angles = {}
        # go through possible angle types
        for i, atr in enumerate(self.angle_types):
            print("Creating angles: %s" % atr)

            cut = split(atr)
            atom1 = atr[:cut[0]]
            atom2 = atr[cut[0]:cut[1]]
            atom3 = atr[cut[1]:]

            angles[i] = create_angles_from_bonds(system.bonds,
                                                 self.select(atom1), self.select(atom2), self.select(atom3),
                                                 self.angle_types[atr])

        #tmp = []
        #for i in angles.keys():
        #    tmp.append(angles[i])
        #system.angles = np.asarray(tmp)
        system.angles = np.vstack((angles[i] for i in angles.keys()))

        system.n_angles = len(system.angles)

        return system


def split(atr):
    cut = []
    for j, i in enumerate(atr):
        if i.isupper() and j > 0:
            cut.append(j)

    return cut
