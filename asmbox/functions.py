#!/usr/bin/env

import numpy as np
import copy
import math
from asmbox.gbb import Gbb
import sys

# ==================================================================================================================== #
# General functions for use. Very wide application possibilities
# ==================================================================================================================== #




def randomly(seq):
    shuffled = list(seq)
    np.random.shuffle(shuffled)
    return iter(shuffled)



def insert_atom(system, xyz, atom_type, charge, resid, bond_type, bonds):

    system = copy.deepcopy(system)

    new_atom_id = len(system.types)
    system.xyz = np.vstack((system.xyz, xyz))
    system.types = np.concatenate((system.types, np.array([atom_type])))
    system.charges = np.concatenate((system.charges, np.array([charge])))
    system.resids = np.concatenate((system.resids, np.array([resid])))

    for bond in bonds:
        system.bonds = np.vstack((system.bonds, np.array([bond_type, new_atom_id + 1, bond + 1])))

    # system.bonds = np.vstack((system.bonds, np.array([bond_type, new_atom_id + 1, bonds[1] + 1])))

    return system



# ==================================================================================================================== #
# Coordinate routines
# ==================================================================================================================== #

def distances(p, pts, box):
    '''    significantly more efficient when wrapping first    '''

    d = np.abs(p - pts)

    res = np.where(d > 0.5 * box.lengths, box.lengths - d, d)

    return np.sqrt((res ** 2).sum(axis=-1))

def wrap_point(xyz, box, pbc=np.array([True, True, True])):
    """Wrap coordinates into box"""
    if any(boundary == False for boundary in pbc):
        axis = np.where(pbc == False)[0]
    else:
        axis = -1

    xyz = copy.deepcopy(xyz)

    for k, c in enumerate(xyz):
        if k != axis:
            if c < box.mins[k]:
                n = math.floor((box.mins[k] - c) / box.lengths[k]) + 1
                xyz[k] += box.lengths[k] * n
            elif c > box.maxs[k]:
                n = math.floor((c - box.maxs[k]) / box.lengths[k]) + 1
                xyz[k] -= box.lengths[k] * n

    return xyz


def wrap_points(xyz, box, pbc=np.array([True, True, True])):
    """Wrap coordinates into box"""

    if any(boundary == False for boundary in pbc):
        axis = np.where(pbc == False)[0]
    else:
        axis = -1

    xyz = copy.deepcopy(xyz)

    for i, coords in enumerate(xyz):
        for k, c in enumerate(coords):
            if k != axis:
                if c < box.mins[k]:
                    n = math.floor((box.mins[k] - c) / box.lengths[k]) + 1
                    xyz[i, k] += box.lengths[k] * n
                elif c > box.maxs[k]:
                    n = math.floor((c - box.maxs[k]) / box.lengths[k]) + 1
                    xyz[i, k] -= box.lengths[k] * n

    return xyz



def vector(p1, p2, box, pbc=np.array([True, True, True])):
    '''
    Function to compute the vectory between two points.

    :param p1:
    :param p2:
    :param box:
    :param pbc:
    :return:
    '''

    return wrap_point(p2 - p1, box, pbc)


def distance(p1, p2, box, pbc=np.array([True, True, True])):
    '''
    Function to compute the distance between two points.

    :param p1:
    :param p2:
    :param box:
    :param pbc:
    :return:
    '''

    vec = vector(p1, p2, box, pbc)

    return np.sqrt((vec ** 2).sum(axis=-1))


def distances(p, pts, box, pbc=np.array([True, True, True])):
    '''
    Function that computes distances between one point and multiple other points

    :param p:
    :param pts:
    :param box:
    :param pbc:
    :return: Can be scalar or array depending on input
    '''

    if len(pts.shape) > 1:
        d = np.zeros(pts.shape[0])
        for i, p2 in enumerate(pts):
            d[i] = distance(p, p2, box, pbc)
    else:
        d = distance(p, pts, box, pbc)

    return d

# the below implementation is faster. But does not account if one axis is not periodic
def calculate_distances(pts1, pts2, boxlength):

    r = np.zeros((pts1.shape[0], pts2.shape[0]))
    for i, j in enumerate(pts1):
        r[i, :] = np.sqrt(((j - pts2)**2).sum(axis=-1))
    return np.where(r > boxlength, r - boxlength/2, r)


# ==================================================================================================================== #
# Bond routines
# ==================================================================================================================== #




def create_bonds_from_distances(system, box, indices1, indices2, distance, pbc=np.array([True, True, True])):
    '''
    Function to compute the bond connections for indices1 and indices2 with distance distance
    Currently Only works on a amorphous silica cell derived with BKS force field

    :param system:
    :param box:
    :param indices1:
    :param indices2:
    :param distance:
    :param pbc:
    :return:
    '''

    indices1 = copy.deepcopy(indices1)
    indices2 = copy.deepcopy(indices2)
    system = copy.deepcopy(system)
    box = copy.deepcopy(box)


    if system.bonds.size:

        print('system.bonds exists')
        cont = input('Are you sure you want to continue? (y/n): ')

        while cont.lower() != 'y':

            if cont.lower() == 'n':

                print('Exiting function without computing bonds')

                return None

            else:
                cont = input('Are you sure you want to continue? (y/n): ')

    c = 0
    bonds = {}

    for i in indices1:

        c += 1
        print('Creating bonds %i\r' % c, end='')

        d = distances(system.xyz[i], system.xyz[indices2], box, pbc)

        indices = np.where(d < distance)[0]

        bonds[i] = list(np.asarray(indices2)[indices])

    print('')

    return bonds



def write_bonds_dictionary_to_array(bonds, indices1, indices2, bond_type):

    '''
    Function to write a dictionary of bonds
    bonds[index1] = [indexbonded1, indexbonded2,..]
    to array format
    [[id1, type, index1, indexbonded1],
    [id2, type, index1, indexbonded2],
    ...]

    Result can be concatenated with system.bonds
    Requieres to create bonds in dictionary first

    is only used to convert non-bonded case to bonded case

    :param bonds:
    :param indices1:
    :param indices2:
    :param type:
    :return:
    '''

    indices1 = copy.deepcopy(indices1)
    indices2 = copy.deepcopy(indices2)

    c = 0
    BONDS = []

    for i in indices1:

        c += 1
        print("Writing bonds dictionary to array: %d \r" % c, end="")

        for j in bonds[i]:

            if j in indices2:

                BONDS.append([bond_type, i+1, j+1])

    print('')

    if BONDS == []:
        BONDS = np.empty(shape=(0, 3), dtype=int)

    return np.asarray(BONDS).astype(int)



def count_bonds(system, id):
    '''
    Function to count number of occurances of id in system.bonds array

    :param system:
    :param id:
    :return:
    '''

    bond_counter = 0

    for j in [[1, 2], [2, 1]]:

        # find if atom index is in column
        k = np.where(system.bonds[:, j[0]] == id + 1)[
            0]  # remember +1 becuase index in python is different than in lammps

        # if atom index is in column enter if statement
        if len(k) > 0:

            # loop through all occurances of atom index in bonds per column
            for b in k:

                # if bonded add to bond counter
                bond_counter += 1

    return bond_counter


def find_bond_partners(system, id):
    '''
    Function to determine the bond partner ids
    Mind that it determines bond partner ID as understood by atom indices
    Not as understood by bonds

    :param system:
    :param id:
    :return:
    '''

    bond_partner = []

    for j in [[1, 2], [2, 1]]:

        # find if atom index is in column
        k = np.where(system.bonds[:, j[0]] == id + 1)[0]  # remember +1 becuase index in python is different than in lammps

        # if atom index is in column enter if statement
        if len(k) > 0:

            # loop through all occurances of atom index in bonds per column
            for b in k:
                # if bonded add to bond counter
                bond_partner.append(system.bonds[b, j[1]]-1)

    return bond_partner



def remove_bonds(system, id):
    '''
    Function to remove all bonds which include atom of specified ID

    :param system:
    :param id:
    :return:
    '''

    system = copy.deepcopy(system)

    remove_ = []

    # iterate through both columns of bonds
    for j in [[1, 2], [2, 1]]:

        # find if atom index is in column
        k = np.where(system.bonds[:, j[0]] == id + 1)[0]  # remember +1 becuase index in python is different than in lammps

        # if atom index is in column enter if statement
        if len(k) > 0:

            # loop through all occurances of atom index in bonds per column
            for b in k:
                remove_.append(b)

    remove_.sort()
    for k in reversed(remove_):
        system.bonds = np.delete(system.bonds, k, 0)

    return system


def silanol_classification(system, atoms):
    indices = atoms.indices['Od']+atoms.indices['Os']
    isolated = []
    vicinal = []
    geminal = []
    vicinal_ = []
    geminal_ = []

    for i in indices:
        # others = copy.deepcopy(atoms.indices['Od']+atoms.indices['Os'])
        # others.remove(i)

        # determine Si partner
        temp_list = find_bond_partners(system, i)
        for tmp in temp_list:
            if tmp in atoms.select('Si'):
                Si = tmp


        # determine all O partners, exclude self
        temp_list = find_bond_partners(system, Si)
        Oxygens = []
        for tmp in temp_list:
            if tmp != i:
                Oxygens.append(tmp)

        # find all Si+1 partners, exclude Si
        otherSilicons = []
        for O in Oxygens:
            temp_list = find_bond_partners(system, O)
            for tmp in temp_list:
                if tmp != Si and tmp in atoms.select('Si'):
                    otherSilicons.append(tmp)

        # find all O+2 partners
        otherOxygens = []
        for otherSi in otherSilicons:
            temp_list = find_bond_partners(system, otherSi)
            for tmp in temp_list:
                if tmp not in Oxygens:
                    otherOxygens.append(tmp)

        # geminal
        if sum(O in indices for O in Oxygens) > 0:
            tmp = []
            for O in Oxygens:
                if O in indices:
                    tmp.append(O)
            geminal_.append(tmp)
            geminal.append(i)

        # vicinals
        elif sum(otherO in indices for otherO in otherOxygens) > 0:
            tmp = []
            for otherO in otherOxygens:
                if otherO in indices:
                    tmp.append(otherO)
            vicinal_.append(tmp)
            vicinal.append(i)

        # isolated
        else:
            isolated.append(i)

    return isolated, vicinal, geminal#, vicinal_, geminal_


def deprotonated_classification(system, box, atoms, distance):
    isolated = []
    vicinal = []
    geminal = []

    for i in atoms.indices['Od']:
        others = copy.deepcopy(atoms.indices['Od'])
        others.remove(i)
        Si = find_bond_partners(system, i)[0]
        Oxygens = find_bond_partners(system, Si)
        if sum(O in atoms.indices['Od'] for O in Oxygens) > 1:
            geminal.append(i)
        elif min(distances(system.xyz[i, :], system.xyz[others, :], box)) < distance:
            vicinal.append(i)
        else:
            isolated.append(i)

    return isolated, vicinal, geminal