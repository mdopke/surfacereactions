#!/usr/bin/env

import numpy as np
from asmbox.gbb import Gbb

def read_lammps_data_file(fileI):

    '''
    Function that uses groupy to read a lammps data file

    For the future implement groupy functions in own library

    :param fileI:
    :return:
    '''

    system = Gbb()
    box = system.load_lammps_data(fileI)

    return system, box


def write_lammps_data_file(filename, gbb, box, FF=True):
    '''
    Function stolen from groupy to ease modification for some force fields

    :param filename:
    :param gbb:
    :param box:
    :return:
    '''

    if (box == None) and np.sum(gbb.box.dims) > 0:
        box = gbb.box
    elif (box == None):
        raise Exception("Box is empty")

    with open(filename, 'w') as f:
        f.write('LAMMPS data file generated with write_lammps_data. Ask author for the file m.f.dopke@tudelft.nl\n')
        f.write('\n')

        n_bonds = int(gbb.bonds.shape[0])
        n_angles = int(gbb.angles.shape[0])
        n_dihedrals = int(gbb.dihedrals.shape[0])
        n_impropers = int(gbb.impropers.shape[0])

        f.write(str(gbb.xyz.shape[0]) + ' atoms\n')
        f.write(str(n_bonds) + ' bonds\n')
        f.write(str(n_angles) + ' angles\n')
        f.write(str(n_dihedrals) + ' dihedrals\n')
        f.write('\n')

        f.write(str(int(max(np.unique(gbb.types)))) + ' atom types\n')
        if n_bonds > 0:
            f.write(str(int(max(np.unique(gbb.bonds[:, 0])))) + ' bond types\n')
        if n_angles > 0:
            f.write(str(int(max(np.unique(gbb.angles[:, 0])))) + ' angle types\n')
        if n_dihedrals > 0:
            f.write(str(int(max(np.unique(gbb.dihedrals[:, 0])))) + ' dihedral types\n')
        f.write('\n')

        f.write('%-8.4f %8.4f xlo xhi\n' % (box.mins[0], box.maxs[0]))
        f.write('%-8.4f %8.4f ylo yhi\n' % (box.mins[1], box.maxs[1]))
        f.write('%-8.4f %8.4f zlo zhi\n' % (box.mins[2], box.maxs[2]))

        if FF:
            if len(gbb.masses) > 0:
                f.write('\n')
                f.write('Masses\n')
                f.write('\n')
                for i, value in sorted(gbb.masses.items()):
                    f.write("%d %8.4f\n" % (i, value))

            if len(gbb.pair_types) > 0:
                f.write('\n')
                f.write('Pair Coeffs\n')
                f.write('\n')
                for i, value in sorted(gbb.pair_types.items()):
                    f.write("%d %8.4e %8.4f\n" % (i, value[0], value[1]))

            # modified to account for more coefficients
            if len(gbb.bond_types) > 0:
                f.write('\n')
                f.write('Bond Coeffs\n')
                f.write('\n')
                # for i, value in sorted(gbb.bond_types.items()):
                #     f.write("%d %8.4f %8.4f\n" % (i, value[0], value[1]))
                for i, value in sorted(gbb.bond_types.items()):
                    f.write("%d " % i)
                    for j in value:
                        f.write("%8.4f " % j)
                    f.write("\n")

            if len(gbb.angle_types) > 0:
                f.write('\n')
                f.write('Angle Coeffs\n')
                f.write('\n')
                # for i, value in sorted(gbb.angle_types.items()):
                #     f.write("%d %8.4f %8.4f\n" % (i, value[0], value[1]))
                for i, value in sorted(gbb.angle_types.items()):
                    f.write("%d " % i)
                    for j in value:
                        f.write("%8.4f " % j)
                    f.write("\n")
        else:
            f.write('\n')
            f.write('Masses\n')
            f.write('\n')
            for i in range(int(max(np.unique(gbb.types)))):
                f.write("%d %8.4f\n" % (i+1, 1.0))

        # if len(gbb.bond_bond_types) > 0:
        #     f.write('\n')
        #     f.write('Angle Coeffs\n')
        #     f.write('\n')
        #     for i, value in sorted(gbb.bond_bond_types.items()):
        #         f.write("%d " % i)
        #         for j in value:
        #             f.write("%8.4f " % j)
        #         f.write("\n")
        #
        # if len(gbb.bond_angle_types) > 0:
        #     f.write('\n')
        #     f.write('Angle Coeffs\n')
        #     f.write('\n')
        #     for i, value in sorted(gbb.bond_angle_types.items()):
        #         f.write("%d " % i)
        #         for j in value:
        #             f.write("%8.4f " % j)
        #         f.write("\n")
        # end modification

        if len(gbb.dihedral_types) > 0:
            f.write('\n')
            f.write('Dihedral Coeffs\n')
            f.write('\n')
            for i, value in sorted(gbb.dihedral_types.items()):
                f.write("%d %8.4f %8.4f %8.4f %8.4f\n" % (i, value[0], value[1], value[2], value[3]))

        f.write('\n')
        f.write('Atoms\n')
        f.write('\n')
        for i, coord in enumerate(gbb.xyz):
            if len(gbb.resids) > 0:
                resid = gbb.resids[i]
            elif len(gbb.resids) == 0:
                resid = 1
            f.write('%-6d %-6d %-6d %5.12f %8.3f %8.3f %8.3f\n'
                    % (i + 1,
                       resid,
                       gbb.types[i],
                       gbb.charges[i],
                       coord[0],
                       coord[1],
                       coord[2]))


        # if gbb.vel.size:
        #     f.write('\n')
        #     f.write('Velocities\n')
        #     f.write('\n')
        #     for i, coord in enumerate(gbb.vel):
        #         f.write('%-6d %8.5f %8.5f %8.5f\n'
        #                 % (i + 1,
        #                    coord[0],
        #                    coord[1],
        #                    coord[2]))
        if n_bonds > 0:
            f.write('\n')
            f.write('Bonds\n')
            f.write('\n')
            for i, bond in enumerate(gbb.bonds):
                f.write(str(i + 1) + " " + " ".join(list(map(str, bond))) + '\n')

        if n_angles > 0:
            f.write('\n')
            f.write('Angles\n')
            f.write('\n')
            for i, angle in enumerate(gbb.angles):
                f.write(str(i + 1) + " " + " ".join(list(map(str, angle))) + '\n')

        if n_dihedrals > 0:
            f.write('\n')
            f.write('Dihedrals\n')
            f.write('\n')
            for i, dihedral in enumerate(gbb.dihedrals):
                f.write(str(i + 1) + " " + " ".join(list(map(str, dihedral))) + '\n')

        if n_impropers > 0:
            f.write('\n')
            f.write('Impropers\n')
            f.write('\n')
            for i, improper in enumerate(gbb.impropers):
                f.write(str(i + 1) + " " + " ".join(list(map(str, improper))) + '\n')

    print("Wrote file '" + filename + "'")