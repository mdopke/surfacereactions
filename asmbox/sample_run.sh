#!/bin/bash
asmbox="/home/mfdopke/Dropbox/Phd/git/asmbox/"
silanolDens=4.6
atom_types="{'Ow': 1, 'Hw': 2, 'Ob': 3, 'Sib': 4, 'Od': 5, 'Sid': 6, 'Sidd': 7, 'Os': 8, 'Sis': 9, 'Siss': 10, 'Hs': 11}"
bond_types="{'OwHw': 1, 'OSi': 2, 'OsHs': 3}"
angle_types="{'HwOwHw': 1, 'SiOSi': 2, 'OSiO': 3, 'SiOH': 4}"

python ${asmbox}tools/lammpstrj_to_lammpsdata.py lammpsinput.data trj.lammps lammps.data
python ${asmbox}tools/nonbonded_to_bonded.py "$atom_types" "$bond_types" "$angle_types"lammps.data lammps.data_bonded_xyz
python ${asmbox}tools/wrap.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_xyz lammps.data_bonded_xyz
python ${asmbox}tools/modify_xyz.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_xyz lammps.data_bonded_zxy
python ${asmbox}tools/modify_xyz.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_zxy lammps.data_bonded_yzx

echo ""
echo "xyz"
echo ""

python ${asmbox}tools/cut.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_xyz lammps.data_cut inside -100 100 -100 100 -3 3
python ${asmbox}tools/add_vacuum_and_adjust_charges.py "$atom_types" "$bond_types" "$angle_types"lammps.data_cut lammps.data_neutral inside -100 100 -100 100 -100 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_neutral lammps.data_bridged $silanolDens inside -100 100 -100 100 -8 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_bridged $silanolDens inside -100 100 -100 100 0 8
python ${asmbox}tools/silanolCharacterization.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged
python ${asmbox}tools/protonate.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_protonated_xyz

echo ""
echo "zxy"
echo ""

python ${asmbox}tools/cut.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_zxy lammps.data_cut inside -100 100 -100 100 -3 3
python ${asmbox}tools/add_vacuum_and_adjust_charges.py "$atom_types" "$bond_types" "$angle_types"lammps.data_cut lammps.data_neutral inside -100 100 -100 100 -100 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_neutral lammps.data_bridged $silanolDens inside -100 100 -100 100 -8 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_bridged $silanolDens inside -100 100 -100 100 0 8
python ${asmbox}tools/silanolCharacterization.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged
python ${asmbox}tools/protonate.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_protonated_zxy

echo ""
echo "yzx"
echo ""

python ${asmbox}tools/cut.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bonded_yzx lammps.data_cut inside -100 100 -100 100 -3 3
python ${asmbox}tools/add_vacuum_and_adjust_charges.py "$atom_types" "$bond_types" "$angle_types"lammps.data_cut lammps.data_neutral inside -100 100 -100 100 -100 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_neutral lammps.data_bridged $silanolDens inside -100 100 -100 100 -8 0
python ${asmbox}tools/adjust_silanol_density.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_bridged $silanolDens inside -100 100 -100 100 0 8
python ${asmbox}tools/silanolCharacterization.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged
python ${asmbox}tools/protonate.py "$atom_types" "$bond_types" "$angle_types"lammps.data_bridged lammps.data_protonated_yzx

#python ${asmbox}tools/separate_walls.py "$atom_types" "$bond_types" "$angle_types"lammps.data_protonated lammps.data_separated 44 2
#python ${asmbox}tools/add_water.py "$atom_types" "$bond_types" "$angle_types"lammps.data_separated lammps.data_water -100 100 -100 100 -21 21 1
#python ${asmbox}tools/add_ions.py "$atom_types" "$bond_types" "$angle_types"lammps.data_electrolyte lammps.data_electrolyte Ca 8  -100 100 -100 100 -6 6
#python ${asmbox}tools/add_ions.py "$atom_types" "$bond_types" "$angle_types"lammps.data_electrolyte lammps.data_electrolyte Na 38  -100 100 -100 100 -6 6
#python ${asmbox}tools/add_ions.py "$atom_types" "$bond_types" "$angle_types"lammps.data_electrolyte lammps.data_electrolyte Cl 38  -100 100 -100 100 -6 6
#python ${asmbox}tools/add_ions.py "$atom_types" "$bond_types" "$angle_types"lammps.data_electrolyte lammps.data_electrolyte Na 19  -100 100 -100 100 -6 6
#python ${asmbox}tools/add_ions.py "$atom_types" "$bond_types" "$angle_types"lammps.data_electrolyte lammps.data_electrolyte Cl 38  -100 100 -100 100 -6 6
#python ${asmbox}tools/add_FF.py "$atom_types" "$bond_types" "$angle_types"lammps.data_water lammps.data_IFF_tip4p2005 interface tip4p2005
#python ${asmbox}tools/add_FF.py "$atom_types" "$bond_types" "$angle_types"lammps.data_water lammps.data_IFF_SPCE interface SPCE

