#!/usr/bin/env

import sys
import numpy as np
from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.functions import create_bonds_from_distances, write_bonds_dictionary_to_array
from asmbox.FF import FF

import ast



if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]



    system, box = read_lammps_data_file(infile)


    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.indices['Ob'] = list(np.where(system.types == 1)[0])
    atoms.indices['Sib'] = list(np.where(system.types == 2)[0])
    # atoms.assign_indices(system)

    atoms.printConfiguration()

    print('Making bonds')
    bonds = create_bonds_from_distances(system, box, atoms.indices['Ob'], atoms.indices['Sib'], 1.8)
    bonds = write_bonds_dictionary_to_array(bonds, atoms.indices['Ob'], atoms.indices['Sib'], 2)

    system.bonds = bonds

    system = atoms.update(system)
    FF.combine(['interface', 'tip4p2005'])
    system = FF.apply(system)

    write_lammps_data_file(outfile, system, box)

