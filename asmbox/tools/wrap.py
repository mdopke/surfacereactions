#!/usr/bin/env

import numpy as np
import copy
import sys

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file


if __name__ == '__main__':

    system, box = read_lammps_data_file(sys.argv[1])
    system.wrap(box)

    write_lammps_data_file(sys.argv[2], system, box)

