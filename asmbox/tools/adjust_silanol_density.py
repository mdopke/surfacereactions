#!/usr/bin/env

import numpy as np
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF

from asmbox.asm import adjust_silanol_density

if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    density = sys.argv[6]
    area = sys.argv[7] #(24.9127 * 2) ** 2
    remove = sys.argv[8]

    try:
        xMin = sys.argv[9]
        xMax = sys.argv[10]
        yMin = sys.argv[11]
        yMax = sys.argv[12]
        zMin = sys.argv[13]
        zMax = sys.argv[14]

        region = np.array([[float(xMin), float(xMax)],
                           [float(yMin), float(yMax)],
                           [float(zMin), float(zMax)]])

    except:
        x0 = sys.argv[9]
        y0 = sys.argv[10]
        z0 = sys.argv[11]
        r = sys.argv[12]

        region = np.array([float(x0), float(y0), float(z0), float(r)])



    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    system, atoms, BO, BSi = adjust_silanol_density(system, box, atoms, float(area), float(density), 1, region, remove=remove)

    system = atoms.update(system)
    FF.combine(['interface', 'tip4p2005'])
    system = FF.apply(system)

    write_lammps_data_file(outfile, system, box)


