from ase import Atoms
from ase.io import read, write
import sys
import copy
import itertools
from asmbox.gbb import Gbb
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
from analysis.functions import init_plotting

init_plotting()


def rdf_ase(data, type1, type2, n_bins=99, r_range=(0.1, 10.0), mic=True):
    # create rdf
    g_r, edges = np.histogram([0], bins=n_bins, range=r_range)
    g_r[0] = 0

    # rho = 0
    atoms_1 = np.where(data.get_array('types')==type1)[0]
    atoms_2 = np.where(data.get_array('types')==type2)[0]
    n_0 = len(atoms_1)
    n_1 = len(atoms_2)
    c = 1
    for i in atoms_1:
        print('Completed: %i %%\r' % (c/len(atoms_1)*100), end='')
        r_ij = data.get_distances(i, atoms_2, mic=mic)

        # add r_ij to histogram and sum to rdf
        temp_g_r, _ = np.histogram(r_ij, bins=n_bins, range=r_range)
        g_r += temp_g_r
        c += 1
    print('')


    # center of bins (r)
    r = 0.5 * (edges[1:] + edges[:-1])
    # sphere volume
    V = 4. / 3. * np.pi * (np.power(edges[1:], 3) - np.power(edges[:-1], 3))
    volume = data.cell[0, 0] * data.cell[1, 1] * data.cell[2, 2]
    rho = n_0 / volume
    norm = rho * n_1
    g_r = g_r / (norm * V )
    return r, g_r



if len(sys.argv) < 4:
    print('Wrong number of inputs.\n Inputs are:\n '
          'Input file, O type (int), Si type (int)')
    exit()
#fileI = '/home/max/Dropbox/Phd/LAMMPS_simulations/channel_preparation/lammps.data_exampleInput'
fileI = sys.argv[1]
fileO = fileI + '_out'
Otype = int(sys.argv[2])
Sitype = int(sys.argv[3])

print('Reading ', fileI)
data = read(fileI, format='lammps-data')
data.set_array('types', data.get_atomic_numbers())


# remove all Si within rectangle
O = list(np.where(data.get_array('types') == Otype)[0])
Si = list(np.where(data.get_array('types') == Sitype)[0])

# Determine coordination number
bonds = {}
for i in O:
    bonds[i] = list(np.asarray(Si)[np.where(data.get_distances(i, Si, mic=True)< 1.8)[0]])
for i in Si:
    bonds[i] = list(np.asarray(O)[np.where(data.get_distances(i, O, mic=True)< 1.8)[0]])

c = 0
c2 = 0
c3 = 0
c4 = 0
c5 = 0
c6 = 0
lenSi = len(Si)
for i in Si:
    c += len(bonds[i])/lenSi
    print('Average Si coordination number: %f\r' % c, end='')
    if len(bonds[i]) < 3:
        c2 += 1
    elif len(bonds[i]) == 3:
        c3 += 1
    elif len(bonds[i]) == 4:
        c4 += 1
    elif len(bonds[i]) == 5:
        c5 += 1
    elif len(bonds[i]) > 5:
        c6 += 1
print('')
print('SiO<3: %i, SiO3: %i, SiO4: %i, SiO5: %i, SiO>5: %i, ' % (c2, c3, c4, c5, c6))

c = 0
c0 = 0
c1 = 0
c2 = 0
c3 = 0
c4 = 0
lenO = len(O)
for i in O:
    c += len(bonds[i])/lenO
    print('Average O coordination number: %f\r' % c, end='')
    if len(bonds[i]) < 1:
        c0 += 1
    elif len(bonds[i]) == 1:
        c1 += 1
    elif len(bonds[i]) == 2:
        c2 += 1
    elif len(bonds[i]) == 3:
        c3 += 1
    elif len(bonds[i]) > 3:
        c4 += 1
print('')
print('OSi<1: %i, OSi1: %i, OSi2: %i, OSi3: %i, OSi>3: %i, ' % (c0, c1, c2, c3, c4))


# plot rdf
print('Computing radial basis functions')
fig, ax = plt.subplots()
r, g = rdf_ase(data, Sitype, Otype, n_bins=2000)
ax.plot(r, gaussian_filter(g, 8), 'k-', label='Si-O')
r, g = rdf_ase(data, Sitype, Sitype, n_bins=2000)
ax.plot(r, gaussian_filter(g, 8), 'b--', label='Si-Si')
r, g = rdf_ase(data, Otype, Otype, n_bins=2000)
ax.plot(r, gaussian_filter(g, 8), 'r-.', label='O-O')
ax.set_xlabel('r', fontsize=14)
ax.set_ylabel('g(r)', fontsize=14)
ax.grid()
plt.legend(fontsize=14)
plt.show()



