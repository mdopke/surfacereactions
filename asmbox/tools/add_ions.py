#!/usr/bin/env

import numpy as np
import copy
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.asm import add_ion
from asmbox.FF import FF



if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    ion = sys.argv[6]
    nIon = sys.argv[7]
    d = sys.argv[8]

    xMin = sys.argv[9]
    xMax = sys.argv[10]
    yMin = sys.argv[11]
    yMax = sys.argv[12]
    zMin = sys.argv[13]
    zMax = sys.argv[14]

    system, box = read_lammps_data_file(infile)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    # FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    cube = np.array([[float(xMin), float(xMax)],
                     [float(yMin), float(yMax)],
                     [float(zMin), float(zMax)]])

    system, atoms = add_ion(system, box, atoms, ion, int(nIon), cube, float(d))

    # FF.combine(['interface', 'SPCE', 'interfaceIons'])
    # system = FF.apply(system)

    write_lammps_data_file(outfile, system, box, FF=False)

