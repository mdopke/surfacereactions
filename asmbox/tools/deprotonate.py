#!/usr/bin/env

import numpy as np
import copy
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF
from asmbox.asm import deprotonate

if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    density = sys.argv[6]
    remove = sys.argv[7]

    xMin = sys.argv[8]
    xMax = sys.argv[9]
    yMin = sys.argv[10]
    yMax = sys.argv[11]
    zMin = sys.argv[12]
    zMax = sys.argv[13]

    cube = np.array([[float(xMin), float(xMax)],
                       [float(yMin), float(yMax)],
                       [float(zMin), float(zMax)]])


    system, box = read_lammps_data_file(infile)
    system.wrap(box)


    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    atoms = deprotonate(system, box, atoms, int(density), cube, remove)

    system = atoms.update(system)
    # FF.combine(['interface', 'tip4p2005'])
    # system = FF.apply(system)

    write_lammps_data_file(outfile, system, box, FF=False)

