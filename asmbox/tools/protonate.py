#!/usr/bin/env

import numpy as np
import copy
import sys
import ast

from asmbox.mdio2 import read_lammps_data_file, write_lammps_data_file
from asmbox.indices import Indices
from asmbox.FF import FF
from asmbox.asm import protonate

if __name__ == '__main__':

    atom_types = ast.literal_eval(sys.argv[1])
    bond_types = ast.literal_eval(sys.argv[2])
    angle_types = ast.literal_eval(sys.argv[3])

    infile = sys.argv[4]
    outfile = sys.argv[5]

    system, box = read_lammps_data_file(infile)
    system.wrap(box)

    atoms = Indices(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)
    FF = FF(atom_types=atom_types, bond_types=bond_types, angle_types=angle_types)

    atoms.assign_indices(system)

    system, atoms = protonate(system, box, atoms, 3)

    system = atoms.update(system)
    FF.combine(['interface', 'tip4p2005'])
    system = FF.apply(system)

    write_lammps_data_file(outfile, system, box)

