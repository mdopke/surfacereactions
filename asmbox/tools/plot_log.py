import numpy as np
import sys
import matplotlib.pyplot as plt
import os
from shutil import copyfile


def read_file(filename):
	file = open(filename, 'r')
	header_len = 0
	reader = 0
	for k, line in enumerate(file):
		if reader == 1:
			try:
				tmp = [float(x) for x in line.split()]
				if len(tmp) == len(header):
					data.append(tmp)
			except:
				reader = 0
		if line.find('Step') == 0:
			print('Reading ' + line)
			header = line.split()
			if len(header) != header_len:
				data = []
			reader = 1
			header_len = len(header)
	file.close()

	return header, np.asarray(data)

	
if __name__ == '__main__':
	header, data = read_file(sys.argv[1])
	print('Select X variable')
	selection = input(header)
	try:
		var_x = int(selection)
	except:
		for k, variable in enumerate(header):
			if variable.lower().find(selection.lower()) == 0:
				var_x = k
	print('Select Y variable')
	selection = input(header)
	try:
		var_y = int(selection)
	except:
		for k, variable in enumerate(header):
			if variable.lower().find(selection.lower()) == 0:
				var_y = k
	try:
		print('Plotting '+ header[var_x] + ' vs ' + header[var_y])
		fig, ax = plt.subplots()
		ax.plot(data[:, var_x], data[:, var_y])
		z = np.polyfit(data[:, var_x], data[:, var_y], 1)
		ax.plot(data[:, var_x], z[0] * data[:, var_x] + z[1])
		ax.set_xlabel(header[var_x])
		ax.set_ylabel(header[var_y])
		ax.grid()
		print('Select filename to save image')
		imagename = input()
		if len(imagename) > 0:
			plt.savefig(imagename)
		plt.show()
	except:
		print('''ERROR:
Possibily several runs in same log file with different headers.''')

