import numpy as np

class coeffs:
    def __init__(self):
        self.masses = {}
        self.charges = {}
        self.pair_coeffs = {}
        self.bond_coeffs = {}
        self.angle_coeffs = {}

class FF:

    def __init__(self, atom_types, bond_types, angle_types):

        self.atom_types = atom_types
        self.bond_types = bond_types
        self.angle_types = angle_types

        self.masses = {}
        self.charges = {}
        self.pair_coeffs = {}
        self.bond_coeffs = {}
        self.angle_coeffs = {}









    # SiO
    def interface(self):

        print('Geometric mixing rule')

        FF = coeffs()
        FF.masses = {
            'Ob': 15.999,
            'Sib': 28.085,
            'Od': 15.999,
            'Sid': 28.085,
            'Sidd': 28.085,
            'Os': 15.999,
            'Sis': 28.085,
            'Siss': 28.085,
            'Hs': 1.008}

        FF.charges = {
            'Ob': -0.55,
            'Sib': 1.1,
            'Od': -0.9,
            'Sid': 0.725,
            'Sidd': 0.350,
            'Os': -0.675,
            'Sis': 1.1,
            'Siss': 1.1,
            'Hs': 0.4}

        # wrong parameters used in first paper
        # FF.pair_coeffs = {
        #     'Ob': [0.054, 3.47],
        #     'Sib': [0.093, 4.15],
        #     'Od': [0.122, 3.47],
        #     'Sid': [0.093, 4.15],
        #     'Sidd': [0.093, 4.15],
        #     'Os': [0.122, 3.47],
        #     'Sis': [0.093, 4.15],
        #     'Siss': [0.093, 4.15],
        #     'Hs': [0.015, 1.085]}

        FF.pair_coeffs = {
            'Ob': [0.054, 3.0914],
            'Sib': [0.093, 3.6972],
            'Od': [0.122, 3.0914],
            'Sid': [0.093, 3.6972],
            'Sidd': [0.093, 3.6972],
            'Os': [0.122, 3.0914],
            'Sis': [0.093, 3.6972],
            'Siss': [0.093, 3.6972],
            'Hs': [0.015, 0.9666]}

        FF.bond_coeffs = {
            'OSi': [285.0, 1.680],
            'OsHs': [495.0, 0.945]}

        FF.angle_coeffs = {
            'SiOSi': [100.0, 149.0],
            'OSiO': [100.0, 109.5],
            'SiOH': [50.0, 115.0]}

        return FF

    def clayFF(self):

        print('Arithmetic mixing rule')

        FF = coeffs()
        FF.masses = {
            'Ob': 15.999,
            'Sib': 28.085,
            'Od': 15.999,
            'Sid': 28.085,
            'Sidd': 28.085,
            'Os': 15.999,
            'Sis': 28.085,
            'Siss': 28.085,
            'Hs': 1.008}

        FF.charges = {
            'Ob': -1.05,
            'Sib': 2.1,
            'Od': -1.525,#-1.525,
            'Sid': 2.1,
            'Sidd': 2.1,
            'Os': -0.95,
            'Sis': 2.1,
            'Siss': 2.1,
            'Hs': 0.4250}

        FF.pair_coeffs = {
            'Ob': [0.1554, 3.1655],
            'Sib': [0.0000018405, 3.30203],
            'Od': [0.1554, 3.1655],
            'Sid': [0.0000018405, 3.30203],
            'Sidd': [0.0000018405, 3.30203],
            'Os': [0.1554, 3.1655],
            'Sis': [0.0000018405, 3.30203],
            'Siss': [0.0000018405, 3.30203],
            'Hs': [0.000, 0.00]}

        FF.bond_coeffs = {
            'OSi': [0.0, 1.680],
            'OsHs': [554.1349, 1.000]}

        FF.angle_coeffs = {
            'SiOSi': [0.0, 149.0],
            'OSiO': [0.0, 109.5],
            'SiOH': [30.0, 109.47]}

        return FF



    # water
    def tip4p2005(self):

        FF = coeffs()

        FF.masses = {
            'Ow': 15.999,
            'Hw': 1.008}

        FF.charges = {
            'Ow': -1.1128,
            'Hw': 0.5564}

        FF.pair_coeffs = {
            'Ow': [0.1852, 3.1589],
            'Hw': [0.000, 0.00]}

        FF.bond_coeffs = {'OwHw': [447.564, 0.9572]}

        FF.angle_coeffs = {'HwOwHw': [50.0, 104.52]}

        return FF

    def SPCFw(self):

        FF = coeffs()

        FF.masses = {
            'Ow': 15.999,
            'Hw': 1.008}

        FF.charges = {
            'Ow': -0.82,
            'Hw': 0.41}

        FF.pair_coeffs = {
            'Ow': [0.1553, 3.166],
            'Hw': [0.000, 0.00]}

        FF.bond_coeffs = {'OwHw': [1059.16, 1.012]}

        FF.angle_coeffs = {'HwOwHw': [75.90, 113.24]}

        return FF



    def SPCE(self):

        FF = coeffs()

        FF.masses = {
            'Ow': 15.999,
            'Hw': 1.008}

        FF.charges = {
            'Ow': -0.8476,
            'Hw': 0.4238}

        FF.pair_coeffs = {
            'Ow': [0.1553, 3.166],
            'Hw': [0.000, 0.00]}

        FF.bond_coeffs = {'OwHw': [447.564, 1.0]}

        FF.angle_coeffs = {'HwOwHw': [50.0, 109.47]}

        return FF

    def tip3p(self):

        FF = coeffs()

        FF.masses = {
            'Ow': 15.999,
            'Hw': 1.008}

        FF.charges = {
            'Ow': -0.8340,
            'Hw': 0.4170}

        FF.pair_coeffs = {
            'Ow': [0.1521, 3.15061],
            'Hw': [0.000, 0.00]}

        FF.bond_coeffs = {'OwHw': [450.0, 0.9572]}

        FF.angle_coeffs = {'HwOwHw': [55.0, 104.52]}

        return FF











    # ions
    def M(self):

        print('Arithmetic mixing rule')

        FF = coeffs()

        FF.masses = {
            'Ca': 40.078}

        FF.charges = {
            'Ca': 2}

        FF.pair_coeffs = {
            'Ca': [0.2247, 2.4100]}  # mamatkulov 2013

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF

    def NaCl_TIP4P2005(self): # JC 2008

        print('Arithmetic mixing rule')
        print('Parameters not yet fully implemented')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990}

        FF.charges = {
            'Cl': -1,
            'Na': 1}

        FF.pair_coeffs = {
            'Cl': [0.0117, 4.9178],
            'Na': [0.1684, 2.1845]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF

    def JC_TIP4PEw(self): # JC 2008

        print('Arithmetic mixing rule')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990}

        FF.charges = {
            'Cl': -1,
            'Na': 1}

        FF.pair_coeffs = {
            'Cl': [0.0117, 4.9178],
            'Na': [0.1684, 2.1845]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF

    def JC_SPCE(self): # JC 2008

        print('Arithmetic mixing rule')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990}

        FF.charges = {
            'Cl': -1,
            'Na': 1}

        FF.pair_coeffs = {
            'Cl': [0.0127850, 4.8305],
            'Na': [0.3526418, 2.1595]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF

    def JC_tip3p(self): # JC 2008

        print('Arithmetic mixing rule')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990}

        FF.charges = {
            'Cl': -1,
            'Na': 1}

        FF.pair_coeffs = {
            'Cl': [0.0355910, 4.4777],
            'Na': [0.0874393, 2.4393]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF

    def SD_SPCE(self):

        print('Unknown mixing rule')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990}

        FF.charges = {
            'Cl': -1,
            'Na': 1}

        FF.pair_coeffs = {
            'Cl': [0.1000, 4.400],
            'Na': [0.1300, 2.350]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF


    def interfaceIons(self):

        print('Geometric mixing rule')

        FF = coeffs()

        FF.masses = {
            'Cl': 35.45,
            'Na': 22.990,
            'Ca': 40.078}

        FF.charges = {
            'Cl': -1,
            'Na': 1,
            'Ca': 2}

        FF.pair_coeffs = {
            'Cl': [0.107000005, 4.4462973121],
            'Na': [0.0939984526, 2.8241530619],
            'Ca': [0.2900, 2.984510246]}

        FF.bond_coeffs = {}

        FF.angle_coeffs = {}

        return FF




    def combine(self, frcs):

        for ff in frcs:
            frc = getattr(self, ff)()
            self.masses = {**self.masses, **frc.masses}
            self.charges = {**self.charges, **frc.charges}
            self.pair_coeffs = {**self.pair_coeffs, **frc.pair_coeffs}
            self.bond_coeffs = {**self.bond_coeffs, **frc.bond_coeffs}
            self.angle_coeffs = {**self.angle_coeffs, **frc.angle_coeffs}

    def apply(self, system):

        system.masses = {}
        system.pair_types = {}
        system.bond_types = {}
        system.angle_types = {}
        for i, atr in enumerate(self.atom_types):
            system.masses[self.atom_types[atr]] = self.masses[atr]
            where = np.where(system.types == self.atom_types[atr])[0]
            system.charges[where] = self.charges[atr]

            system.pair_types[self.atom_types[atr]] = self.pair_coeffs[atr]

        for i, atr in enumerate(self.bond_types):
            system.bond_types[self.bond_types[atr]] = self.bond_coeffs[atr]

        for i, atr in enumerate(self.angle_types):
            system.angle_types[self.angle_types[atr]] = self.angle_coeffs[atr]

        return system